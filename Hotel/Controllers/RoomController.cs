﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Hotel.Entities;
using Hotel.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Hotel.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class RoomController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IConfiguration _configuration;
        private readonly IHostingEnvironment _appEnvironment;

        public RoomController(UserManager<ApplicationUser> userManager, ApplicationDbContext context, IConfiguration configuration, IHostingEnvironment appEnvironment)
        {
            _userManager = userManager;
            _context = context;
            _configuration = configuration;
            _appEnvironment = appEnvironment;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var rooms = _context.Rooms.Where(x => !x.IsDelete).Include(x=>x.RoomType).Include(x=>x.RoomPhotos).Include(x=>x.OrderRooms).OrderBy(x => x.Floor).ThenBy(x=>x.RoomNo).ToList();
            var infos = GetRoomsInfo(rooms);

            return Ok(infos);
        }

        [HttpPost]
        public IActionResult Create(Room room)
        {
            var userId = _userManager.GetUserId(User);
            room.Id = Guid.NewGuid();
            room.IsDelete = false;
            room.CreatedOn = DateTime.UtcNow.AddHours(6).AddMinutes(30);
            room.ModifiedOn = DateTime.UtcNow.AddHours(6).AddMinutes(30);
            room.CreatedBy = userId;
            room.ModifiedBy = userId;

            var count = 0;

            foreach(var photo in room.RoomPhotos)
            {
                count++;

                string result = Regex.Replace(photo.PhotoName, "^data:image/[a-zA-Z]+;base64,", string.Empty);
                byte[] byt = Convert.FromBase64String(result);

                string filename = Guid.NewGuid().ToString() + ".jpg";
                var save_path = Path.Combine(_appEnvironment.WebRootPath, "images\\rooms\\");

                string img_path = Path.Combine(save_path, filename);
                System.IO.File.WriteAllBytes(img_path, byt);
                //physical image saving ends

                //save image name in database
                photo.Id = Guid.NewGuid();
                photo.PhotoName = filename;
                photo.PhotoOrder = count;
                photo.RoomId = room.Id;
            }

            _context.Add(room);
            _context.SaveChanges();

            return Ok(room);
        }
        
        public List<Object> GetRoomsInfo(List<Room> rooms)
        {
            List<Object> obj = new List<Object>();
            var todayDate = DateTime.UtcNow.AddHours(6).AddMinutes(30);
            foreach (var room in rooms)
            {
                var currentStatus = "";
                var requestStatus = "";
                var reservationStatus = "";

                var checkin = room.OrderRooms.Where(x => x.StartDate <= todayDate && x.EndDate >= todayDate && x.Status == OrderRoom.OrderRoomStatus.CheckedIn);
                currentStatus = checkin == null || checkin.Count()==0 ? "Clear" : "Checked In (CheckOut Date - " + checkin.FirstOrDefault().EndDate.Date + " )";

                var requests = room.OrderRooms.Where(x => x.StartDate >= todayDate && x.Status == OrderRoom.OrderRoomStatus.Requested);
                requestStatus = requests == null || requests.Count() == 0 ?  "No Further Requests" : requests.Count() + " Pending Requests";

                var reservations = room.OrderRooms.Where(x => x.StartDate >= todayDate && x.Status == OrderRoom.OrderRoomStatus.Reserved);
                reservationStatus = reservations == null || reservations.Count()==0 ? "No Upcoming Reservations" : reservations.Count() + " Upcoming Reservations";

                List<OrderRoomItem> orderRoomItems = new List<OrderRoomItem>();
                List<OrderRoomService> orderRoomServices = new List<OrderRoomService>();
                var data = new
                {
                    room,
                    checkin,
                    requests,
                    reservations,
                };

                obj.Add(data);
            }

            return obj;
        }

        public List<Object> GetRoomsInfoForOrder()
        {
            var rooms = _context.Rooms.Where(x => !x.IsDelete).Include(x => x.RoomType).Include(x => x.RoomPhotos).Include(x => x.OrderRooms).OrderBy(x => x.Floor).ToList();
            List<Object> obj = new List<Object>();
            var todayDate = DateTime.UtcNow.AddHours(6).AddMinutes(30);
            foreach (var room in rooms)
            {
                var currentStatus = "";
                var requestStatus = "";
                var reservationStatus = "";

                var checkin = room.OrderRooms.Where(x => x.StartDate >= todayDate && x.EndDate <= todayDate && x.Status == OrderRoom.OrderRoomStatus.CheckedIn);
                currentStatus = checkin == null || checkin.Count() == 0 ? "Clear" : "Checked In (CheckOut Date - " + checkin.FirstOrDefault().EndDate.Date + " )";

                var requests = room.OrderRooms.Where(x => x.StartDate >= todayDate && x.Status == OrderRoom.OrderRoomStatus.Requested);
                requestStatus = requests == null || requests.Count() == 0 ? "No Further Requests" : requests.Count() + " Pending Requests";

                var reservations = room.OrderRooms.Where(x => x.StartDate >= todayDate && x.Status == OrderRoom.OrderRoomStatus.Reserved);
                reservationStatus = reservations == null || reservations.Count() == 0 ? "No Upcoming Reservations" : reservations.Count() + " Upcoming Reservations";

                List<OrderRoomItem> orderRoomItems = new List<OrderRoomItem>();
                List<OrderRoomService> orderRoomServices = new List<OrderRoomService>();
                var startDate = "";
                var endDate = "";
                var amount = 0;
                var data = new
                {
                    room,
                    checkin,
                    requests,
                    reservations,
                    orderRoomItems,
                    orderRoomServices,
                    startDate,
                    endDate,
                    amount
                };

                obj.Add(data);
            }

            return obj;
        }
    }
}