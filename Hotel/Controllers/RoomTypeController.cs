﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Hotel.Entities;
using Hotel.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Hotel.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class RoomTypeController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IConfiguration _configuration;

        public RoomTypeController(UserManager<ApplicationUser> userManager, ApplicationDbContext context, IConfiguration configuration)
        {
            _userManager = userManager;
            _context = context;
            _configuration = configuration;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var roomTypes = _context.RoomTypes.Where(x => !x.IsDelete).Include(x=>x.RoomTypeItems).Include(x=>x.RoomTypeServices).OrderByDescending(x => x.CreatedOn);

            return Ok(roomTypes);
        }

        [HttpPost]
        public IActionResult Create(RoomType roomType)
        {
            var userId = _userManager.GetUserId(User);

            roomType.Id = Guid.NewGuid();
            roomType.CreatedOn = DateTime.UtcNow.AddHours(6).AddMinutes(30);
            roomType.ModifiedOn = DateTime.UtcNow.AddHours(6).AddMinutes(30);
            roomType.CreatedBy = userId;
            roomType.ModifiedBy = userId;

            _context.Add(roomType);
            _context.SaveChanges();

            return Ok(roomType);

        }

        [HttpGet]
        public IActionResult Detail(Guid id)
        {
            var roomType = _context.RoomTypes.Include(x => x.RoomTypeItems).Include(x=>x.RoomTypeServices).Where(x => x.Id == id && !x.IsDelete).FirstOrDefault();
            if (roomType != null)
            {
                return Ok(roomType);
            }

            return NotFound();
        }

        [HttpGet]
        public IActionResult GetItems()
        {
            List<object> data = new List<object>();
            var items = _context.Items.Include(x=>x.PackingUnits).Where(x => !x.IsDelete);
            var packingunits = _context.PackingUnits;

            foreach(var item in items)
            {
                List<object> punits = new List<object>();
                foreach (var punit in item.PackingUnits)
                {
                    var unitname = _context.Units.Find(punit.UnitID).Description;
                    var unittoadd = new
                    {
                        id = punit.UnitID,
                        name = unitname,
                        unitOrder = punit.UnitOrder,
                        qtyInParent = punit.QuantityInParent,
                        saleamount = punit.SaleAmount,
                        selected = ""
                    };
                    punits.Add(unittoadd);
                }
                var obj = new
                {
                    itemId = item.Id,
                    itemCode = item.Code,
                    itemName = item.Name,
                    packingUnitID = "",
                    unitPrice = 0,
                    quantity = 0,
                    amount = 0,
                    sortorder = 0,
                    qtyInSmallestUnit = 0,
                    units = punits
                };
                data.Add(obj);
            }

            return Ok(data);
        }

        [HttpGet]
        public IActionResult GetServices()
        {
            var services = _context.Services;
            return Ok(services);
        }

        [HttpPost]
        public IActionResult Delete(Guid id)
        {
            var roomType = _context.RoomTypes.Find(id);
            if (roomType != null)
            {
                roomType.IsDelete = true;
                _context.SaveChanges();

                return Ok(roomType);
            }

            return NotFound();
        }
    }
}