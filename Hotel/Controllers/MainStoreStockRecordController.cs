﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Hotel.Entities;
using Hotel.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Hotel.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MainStoreStockRecordController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IConfiguration _configuration;

        public MainStoreStockRecordController(UserManager<ApplicationUser> userManager, ApplicationDbContext context, IConfiguration configuration)
        {
            _userManager = userManager;
            _context = context;
            _configuration = configuration;
        }

        [HttpGet]
        public IActionResult Index(string context="")
        {
            var records = _context.MainStoreStockRecords.Where(x => string.IsNullOrEmpty(context) ? true : (x.Item.Code.Contains(context) || x.Item.Name.Contains(context))).OrderByDescending(x => x.StockDate).Include(m => m.MainStore).Include(m => m.Item);

            return Ok(records);
        }

        [HttpPost]
        public async Task<IActionResult> Create(MainStoreStockRecord mainStoreStockRecord)
        {
            mainStoreStockRecord.Id = Guid.NewGuid();
            mainStoreStockRecord.CreatedBy = _userManager.GetUserId(User);
            mainStoreStockRecord.CreatedOn = DateTime.UtcNow.AddHours(6).AddMinutes(30);
            var packingUnits = _context.PackingUnits.Where(x => x.ItemId == mainStoreStockRecord.ItemId).ToList();
            mainStoreStockRecord.QuantityInSmallestUnit = mainStoreStockRecord.Quantity;
            mainStoreStockRecord.UnitName = "";
            if (packingUnits != null)
            {
                var unitOrder = packingUnits.Where(x => x.UnitID == mainStoreStockRecord.UnitID).First().UnitOrder;
                var qty = 1;
                for (var i = packingUnits.Max(x => x.UnitOrder); i > unitOrder; i--)
                {
                    qty *= packingUnits[i - 1].QuantityInParent;
                }
                mainStoreStockRecord.QuantityInSmallestUnit *= qty;
                mainStoreStockRecord.UnitName = _context.Units.Where(x => x.ID == mainStoreStockRecord.UnitID).First().Description;
            }
            _context.Add(mainStoreStockRecord);
            await _context.SaveChangesAsync();

            var mainStoreItem = _context.MainStoreItems.Where(x => x.ItemId == mainStoreStockRecord.ItemId && x.MainStoreId == mainStoreStockRecord.MainStoreId).SingleOrDefault();
            if (mainStoreItem != null)
            {
                mainStoreItem.Quantity += mainStoreStockRecord.QuantityInSmallestUnit;
            }
            else
            {
                MainStoreItem newMainStoreItem = new MainStoreItem();
                newMainStoreItem.ID = Guid.NewGuid();
                newMainStoreItem.MainStoreId = mainStoreStockRecord.MainStoreId;
                newMainStoreItem.ItemId = mainStoreStockRecord.ItemId;
                newMainStoreItem.Quantity = mainStoreStockRecord.QuantityInSmallestUnit;
                _context.MainStoreItems.Add(newMainStoreItem);
            }
            await _context.SaveChangesAsync();

            return Ok(mainStoreStockRecord);
        }
    }
}