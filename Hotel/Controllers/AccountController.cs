﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Hotel.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace Hotel.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly SignInManager<IdentityUser> _signInManager;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly IConfiguration _configuration;
        private readonly ApplicationDbContext _context;

        public AccountController(
            UserManager<IdentityUser> userManager,
            SignInManager<IdentityUser> signInManager,
            IConfiguration configuration,
            ApplicationDbContext context
            )
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _configuration = configuration;
            _context = context;
        }

        [HttpPost]
        public async Task<object> Login([FromBody] LoginDto model)
        {
            var result = await _signInManager.PasswordSignInAsync(model.Username, model.Password, false, false);

            if (result.Succeeded)
            {
                var appUser = _userManager.Users.SingleOrDefault(r => r.UserName == model.Username);
                return GenerateJwtToken(model.Username, appUser);
            }
            return Ok(null);
            //throw new ApplicationException("INVALID_LOGIN_ATTEMPT");
        }

        [HttpPost]
        public async Task<IActionResult> Logout(string returnUrl = null)
        {
            await _signInManager.SignOutAsync();
            return Ok();
        }

        [HttpPost]
        public async Task<object> Register([FromBody] RegisterDto model)
        {
            var user = new IdentityUser
            {
                UserName = model.Username,
                PhoneNumber = model.Username
            };
            var result = await _userManager.CreateAsync(user, model.Password);

            if (result.Succeeded)
            {

                await _userManager.AddToRoleAsync(user, "Customer");

                await _signInManager.SignInAsync(user, false);
                return GenerateJwtToken(model.Username, user);
            }
            else
            {
                var error = result.Errors.ToList()[0].Code;
                if (error == "DuplicateUserName")
                {
                    return null;
                }
            }
            throw new ApplicationException("UNKNOWN_ERROR");
        }

        [HttpPost]
        public async Task<Object> RegisterAdmin([FromBody] RegisterDto model)
        {
            var user = new IdentityUser
            {
                UserName = model.Username,
                Email = model.Username
            };
            var result = await _userManager.CreateAsync(user, model.Password);

            if (result.Succeeded)
            {

                await _userManager.AddToRoleAsync(user, "Admin");

                await _signInManager.SignInAsync(user, false);
                return GenerateJwtToken(model.Username, user);
            }
            else
            {
                var error = result.Errors.ToList()[0].Code;
                if (error == "DuplicateUserName")
                {
                    return null;
                }
            }
            throw new ApplicationException("UNKNOWN_ERROR");
        }

        [HttpPost]
        public async Task<Object> RegisterStaff([FromBody] RegisterDto model)
        {
            var user = new IdentityUser
            {
                UserName = model.Username,
                Email = model.Username
            };
            var result = await _userManager.CreateAsync(user, model.Password);

            if (result.Succeeded)
            {

                await _userManager.AddToRoleAsync(user, "Staff");

                await _signInManager.SignInAsync(user, false);
                return GenerateJwtToken(model.Username, user);
            }
            else
            {
                var error = result.Errors.ToList()[0].Code;
                if (error == "DuplicateUserName")
                {
                    return null;
                }
            }
            throw new ApplicationException("UNKNOWN_ERROR");
        }

        [HttpPost]
        public async Task<Object> RegisterBoss([FromBody] RegisterDto model)
        {
            var user = new IdentityUser
            {
                UserName = model.Username,
                Email = model.Username
            };
            var result = await _userManager.CreateAsync(user, model.Password);

            if (result.Succeeded)
            {

                await _userManager.AddToRoleAsync(user, "Boss");

                await _signInManager.SignInAsync(user, false);
                return GenerateJwtToken(model.Username, user);
            }
            else
            {
                var error = result.Errors.ToList()[0].Code;
                if (error == "DuplicateUserName")
                {
                    return null;
                }
            }
            throw new ApplicationException("UNKNOWN_ERROR");
        }

        public IActionResult checkAllUserNames(string username)
        {
            username = "+" + username.Trim();
            var message = new
            {
                code = 1,
                message = ""
            };
            var existed = false;
            var users = _userManager.Users.Select(x => x.UserName);
            foreach (var user in users)
            {
                if (user == username)
                {
                    existed = true;
                    break;
                }
            }

            if (existed)
            {
                message = new
                {
                    code = 2,
                    message = "This phone number is already existed"
                };
            }

            return Ok(message);
        }

        public async Task<string> getUserRole()
        {
            var userid = _userManager.GetUserId(User);
            IdentityUser user = await _userManager.FindByIdAsync(userid);

            var roles = _userManager.GetRolesAsync(user);

            var role = roles.Result.Count > 0 ? roles.Result[0] : "";

            var rolecode = role.Equals("Admin") ? "192" : (role.Equals("Staff") ? "168" : (role.Equals("Customer") ? "12" : role.Equals("Boss") ? "0" : ""));

            return rolecode;

        }
        [HttpPost]
        public async Task<object> ChangePassword(ChangePasswordViewModel model)
        {

            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            var changePasswordResult = await _userManager.ChangePasswordAsync(user, model.OldPassword, model.NewPassword);
            if (changePasswordResult.Succeeded)
            {
                await _signInManager.SignInAsync(user, false);
                return GenerateJwtToken(user.UserName, user);
            }
            else
            {
                return Ok(null);
            }

            //await _signInManager.SignInAsync(user, isPersistent: false);
            //_logger.LogInformation("User changed their password successfully.");
            //StatusMessage = "Your password has been changed.";

            //return RedirectToAction(nameof(ChangePassword));
        }
        public async Task<object> updatePassword([FromBody] LoginDto model)
        {
            IdentityUser user = await _userManager.FindByNameAsync(model.Username);
            string resetToken = await _userManager.GeneratePasswordResetTokenAsync(user);
            await _userManager.ResetPasswordAsync(user, resetToken, model.Password);
            await _signInManager.SignInAsync(user, false);
            return GenerateJwtToken(model.Username, user);
        }

        private object GenerateJwtToken(string username, IdentityUser user)
        {
            var roles = _userManager.GetRolesAsync(user);

            var role = roles.Result.Count > 0 ? roles.Result[0] : "";

            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, username),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(ClaimTypes.NameIdentifier, user.Id),
                new Claim(ClaimTypes.Role, role),
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JwtKey"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var expires = DateTime.Now.AddDays(Convert.ToDouble(_configuration["JwtExpireDays"]));

            var token = new JwtSecurityToken(
                _configuration["JwtIssuer"],
                _configuration["JwtIssuer"],
                claims,
                expires: expires,
                signingCredentials: creds
            );

            return new JwtSecurityTokenHandler().WriteToken(token);

        }

        private object GenerateJwtToken(string username, IdentityUser user, List<string> role)
        {
            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, username),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(ClaimTypes.NameIdentifier, user.Id),
            };
            for (int i = 0; i < role.Count; i++)
            {
                claims.Add(new Claim(ClaimTypes.Role, role[i]));
            }

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JwtKey"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var expires = DateTime.Now.AddDays(Convert.ToDouble(_configuration["JwtExpireDays"]));

            var token = new JwtSecurityToken(
                _configuration["JwtIssuer"],
                _configuration["JwtIssuer"],
                claims,
                expires: expires,
                signingCredentials: creds
            );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        public class LoginDto
        {
            [Required]
            public string Username { get; set; }

            [Required]
            public string Password { get; set; }

        }

        public class RegisterDto
        {
            [Required]
            public string Username { get; set; }

            [Required]
            [StringLength(100, ErrorMessage = "PASSWORD_MIN_LENGTH", MinimumLength = 6)]
            public string Password { get; set; }

            //public Customer Customer { get; set; }

        }

        public class ChangePasswordViewModel
        {
            [Required]
            [DataType(DataType.Password)]
            [Display(Name = "Current password")]
            public string OldPassword { get; set; }

            [Required]
            [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 6)]
            [DataType(DataType.Password)]
            [Display(Name = "New password")]
            public string NewPassword { get; set; }

            [DataType(DataType.Password)]
            [Display(Name = "Confirm new password")]
            [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
            public string ConfirmPassword { get; set; }

            public string StatusMessage { get; set; }
        }
    }
}