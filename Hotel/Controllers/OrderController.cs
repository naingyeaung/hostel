﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Hotel.Entities;
using Hotel.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Hotel.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IConfiguration _configuration;
        private readonly IHostingEnvironment _appEnvironment;

        public OrderController(UserManager<ApplicationUser> userManager, ApplicationDbContext context, IConfiguration configuration, IHostingEnvironment appEnvironment)
        {
            _userManager = userManager;
            _context = context;
            _configuration = configuration;
            _appEnvironment = appEnvironment;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var orders = _context.Orders.Include(x => x.OrderRooms).OrderByDescending(x=>x.CreatedOn);

            return Ok(orders);
        }

        [HttpPost]
        public IActionResult Create(Order order)
        {
            order.Id = Guid.NewGuid();
            order.CreatedOn = DateTime.UtcNow.AddHours(6).AddMinutes(30);

            _context.Add(order);
            _context.SaveChanges();

            return Ok(order);
        }

        [HttpGet]
        public IActionResult Detail(Guid id)
        {
            var order = _context.Orders.Include(x=>x.OrderRooms).Where(x => x.Id == id).FirstOrDefault();
            if (order != null)
            {
                foreach(var room in order.OrderRooms)
                {
                    room.OrderRoomItems = _context.OrderRoomItems.Include(x=>x.Unit).Where(x => x.OrderRoomId == room.Id).ToList();
                    room.Room = _context.Rooms.Include(x => x.RoomType).Where(x => x.Id == room.RoomId).FirstOrDefault();
                    room.OrderRoomServices = _context.OrderRoomServices.Where(x => x.OrderRoomId == room.Id).ToList();
                }

                return Ok(order);
            }

            return NotFound();
        }

        [HttpPut]
        public IActionResult Edit([FromQuery]Guid id, [FromBody]Order newOrder)
        {
            var order = _context.Orders.Include(x => x.OrderRooms).Where(x => x.Id == id).FirstOrDefault();
            foreach(var room in order.OrderRooms)
            {
                var newRoom = newOrder.OrderRooms.Where(x => x.RoomId == room.RoomId).FirstOrDefault();
                var oldRoom = _context.OrderRooms.Where(x => x.Id == room.Id).FirstOrDefault();
                oldRoom.Status = newRoom.Status;
                _context.SaveChanges();
            }

            return Ok(newOrder);


        }

    }
}