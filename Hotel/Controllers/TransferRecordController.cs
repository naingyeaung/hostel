﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Hotel.Entities;
using Hotel.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Hotel.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransferRecordController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IConfiguration _configuration;

        public TransferRecordController(UserManager<ApplicationUser> userManager, ApplicationDbContext context, IConfiguration configuration)
        {
            _userManager = userManager;
            _context = context;
            _configuration = configuration;
        }

        public IActionResult Index(string context="")
        {
            var records = _context.TransferRecords.Include(t => t.MainStore).Include(t => t.Room).Include(x => x.TransferItems).OrderByDescending(x => x.ModifiedOn);

            return Ok(records);
        }

        public async Task<IActionResult> Create(TransferRecord transferRecord)
        {
            List<string> overQuantityItems = new List<string>();
            foreach (var tmed in transferRecord.TransferItems)
            {
                var roomItem = _context.RoomItems.Where(x => x.ItemId == tmed.ItemId && x.RoomId == transferRecord.RoomId).SingleOrDefault();

                var mainStoreItem = _context.MainStoreItems.Where(x => x.ItemId == tmed.ItemId && x.ItemId == transferRecord.MainStoreID).Include(x => x.Item).SingleOrDefault();
                if (mainStoreItem != null)
                {
                    mainStoreItem.Quantity -= tmed.QuantityInSmallestUnit;
                    if (mainStoreItem.Quantity < 0)
                    {
                        overQuantityItems.Add(mainStoreItem.Item.Code);
                    }
                }

                if (roomItem != null)
                {
                    roomItem.Quantity += tmed.QuantityInSmallestUnit;
                }
                else
                {
                    RoomItem newRoomItem = new RoomItem();
                    newRoomItem.ID = Guid.NewGuid();
                    newRoomItem.RoomId = transferRecord.RoomId;
                    newRoomItem.ItemId = tmed.ItemId;
                    newRoomItem.Quantity = tmed.QuantityInSmallestUnit;
                    _context.RoomItems.Add(newRoomItem);
                }
            }
            if (overQuantityItems.Count() > 0)
            {
                string error = "";
                var i = 0;
                foreach (var item in overQuantityItems.Distinct())
                {
                    error += i == 0 ? item : "," + item;
                    i++;
                }
                if (error != "")
                {
                    error = "The Quantity of " + error + " (is)are over limit.";
                }

                return Ok(error);
            }

            transferRecord.ID = Guid.NewGuid();
            transferRecord.CreatedBy = _userManager.GetUserId(User);
            transferRecord.CreatedOn = DateTime.UtcNow.AddHours(6).AddMinutes(30);
            _context.Add(transferRecord);
            await _context.SaveChangesAsync();

            return Ok(transferRecord);
        }
    }
}