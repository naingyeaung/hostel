﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Hotel.Entities;
using Hotel.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace Hotel.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class UnitController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IConfiguration _configuration;

        public UnitController(UserManager<ApplicationUser> userManager, ApplicationDbContext context, IConfiguration configuration)
        {
            _userManager = userManager;
            _context = context;
            _configuration = configuration;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var units = _context.Units.Where(x => !x.IsDelete).OrderBy(x => x.UnitOrder).ThenBy(x => x.Description).ToList();
            return Ok(units);
        }

        [HttpPost]
        public async Task<IActionResult> Create(Unit unit)
        {
            unit.ID = Guid.NewGuid();
            unit.CreatedBy = _userManager.GetUserId(User);
            unit.CreatedOn = DateTime.UtcNow.AddHours(6).AddMinutes(30);
            _context.Add(unit);
            await _context.SaveChangesAsync();

            return Ok(unit);
        }
    }
}