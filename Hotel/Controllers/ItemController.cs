﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Hotel.Entities;
using Hotel.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Hotel.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ItemController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IConfiguration _configuration;

        public ItemController(UserManager<ApplicationUser> userManager, ApplicationDbContext context, IConfiguration configuration)
        {
            _userManager = userManager;
            _context = context;
            _configuration = configuration;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var items = _context.Items.Where(x => !x.IsDelete).OrderByDescending(x => x.CreatedOn);
            return Ok(items);
        }

        [HttpGet]
        public IActionResult Detail(Guid id)
        {
            var item = _context.Items.Include(x=>x.PackingUnits).Where(x => x.Id == id && !x.IsDelete).FirstOrDefault();
            if (item != null)
            {
                return Ok(item);
            }

            return NotFound();
        }

        [HttpPost]
        public IActionResult Create(Item item)
        {
            item.Id = Guid.NewGuid();
            item.CreatedOn = DateTime.UtcNow.AddHours(6).AddMinutes(30);
            item.ModifiedOn = DateTime.UtcNow.AddHours(6).AddMinutes(30);

            var counter = _context.Counters.Where(x => x.Type == Counter.CounterType.ItemCounter).First();
            var itemCount = counter.Count + 1;
            counter.Count = itemCount;
            item.Code = itemCount.ToString("00000");

            _context.Add(item);
            _context.SaveChanges();

            return Ok(item);
        }

        [HttpPost]
        public IActionResult Delete(Guid id)
        {
            var item = _context.Items.Find(id);
            if (item == null)
            {
                return NotFound();
            }

            item.IsDelete = true;
            _context.SaveChanges();

            return Ok(item);
        }
    }
}