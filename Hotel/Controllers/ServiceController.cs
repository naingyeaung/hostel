﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Hotel.Entities;
using Hotel.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace Hotel.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ServiceController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IConfiguration _configuration;

        public ServiceController(UserManager<ApplicationUser> userManager, ApplicationDbContext context, IConfiguration configuration)
        {
            _userManager = userManager;
            _context = context;
            _configuration = configuration;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var services = _context.Services.Where(x => !x.IsDelete).OrderByDescending(x => x.CreatedOn);

            return Ok(services);
        }

        [HttpPost]
        public IActionResult Create(Service service)
        {
            var userId = _userManager.GetUserId(User);

            service.Id = Guid.NewGuid();
            service.CreatedOn = DateTime.UtcNow.AddHours(6).AddMinutes(30);
            service.ModifiedOn = DateTime.UtcNow.AddHours(6).AddMinutes(30);
            service.CreatedBy = userId;
            service.ModifiedBy = userId;

            _context.Add(service);
            _context.SaveChanges();

            return Ok(service);
        }

        [HttpGet]
        public IActionResult Detail(Guid id)
        {
            var service = _context.Services.Where(x => x.Id == id && !x.IsDelete).FirstOrDefault();
            if (service != null)
            {
                return Ok(service);
            }

            return NotFound();
        }

    }
}