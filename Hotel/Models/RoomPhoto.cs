﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hotel.Models
{
    public class RoomPhoto
    {
        public Guid Id { get; set; }

        public Guid RoomId { get; set; }

        public string PhotoName { get; set; }

        public int PhotoOrder { get; set; }

        //foreignkey table
        public Room Room { get; set; }

        public RoomPhoto()
        {

        }
    }
}
