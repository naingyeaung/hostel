﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hotel.Models
{
    public class OrderRoomService
    {
        public Guid Id { get; set; }

        public Guid OrderRoomId { get; set; }

        public Guid ServiceId { get; set; }

        public int Qty { get; set; }

        public double PricePerService { get; set; }

        public bool IsFOC { get; set; }

        //foreignkey table
        public OrderRoom OrderRoom { get; set; }

        public Service Service { get; set; }

        public OrderRoomService()
        {

        }
    }
}
