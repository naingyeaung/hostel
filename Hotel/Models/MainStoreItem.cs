﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Hotel.Models
{
    public class MainStoreItem
    {
        public Guid ID { get; set; }

        [Required]
        public Guid MainStoreId { get; set; }

        [Required]
        public Guid ItemId { get; set; }

        [Required]
        public int Quantity { get; set; }

        // Foreign Key Tables
        public MainStore MainStore { get; set; }

        public Item Item { get; set; }

        public MainStoreItem()
        {
            
        }
    }
}
