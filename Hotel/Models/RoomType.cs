﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hotel.Models
{
    public class RoomType
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public int AvailableBedQty { get; set; }

        public int AvailableExtraBedQty { get; set; }

        public double RoomPrice { get; set; }

        public double PricePerBed { get; set; }

        public double PricePerExtraBed { get; set; }       

        public bool IsSharable { get; set; }

        public bool IsDelete { get; set; }

        public DateTime CreatedOn { get; set; }

        public DateTime ModifiedOn { get; set; }

        public string CreatedBy { get; set; }

        public string ModifiedBy { get; set; }

        //foreignkey table
        public ICollection<RoomTypeItem> RoomTypeItems { get; set; }

        public ICollection<RoomTypeService> RoomTypeServices { get; set; }

        public RoomType()
        {

        }
    }
}
