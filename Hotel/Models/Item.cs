﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hotel.Models
{
    public class Item
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public double UnitPrice { get; set; }

        public string Code { get; set; }

        public int PercentageForSale { get; set; }

        public bool IsDelete { get; set; }

        public DateTime CreatedOn { get; set; }

        public string CreatedBy { get; set; }

        public DateTime ModifiedOn { get; set; }

        public string ModifiedBy { get; set; }

        public ICollection<PackingUnit> PackingUnits { get; set; }

        public ICollection<MainStoreItem> MainStoreItems { get; set; }

        public ICollection<TransferItem> TransferItems { get; set; }

        public ICollection<OrderRoomItem> OrderRoomItems { get; set; }

        public Item()
        {

        }
    }
}
