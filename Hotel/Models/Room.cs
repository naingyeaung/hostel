﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hotel.Models
{
    public class Room
    {
        public Guid Id { get; set; }

        public Guid RoomTypeId { get; set; }

        public string RoomNo { get; set; }

        public RoomFloor Floor { get; set; }

        public bool IsDelete { get; set; }

        public DateTime CreatedOn { get; set; }

        public DateTime ModifiedOn { get; set; }

        public string CreatedBy { get; set; }

        public string ModifiedBy { get; set; }

        //foreignkey table
        public RoomType RoomType { get; set; }

        public ICollection<RoomPhoto> RoomPhotos { get; set; }

        public ICollection<OrderRoom> OrderRooms { get; set; }

        public enum RoomFloor
        {
            Ground = 1,
            First = 2,
            Second = 3
        }
    }
}
