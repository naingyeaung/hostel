﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Hotel.Models
{
    public class TransferItem
    {
        public Guid Id { get; set; }

        //[Key]
        public Guid TransferRecordId { get; set; }

        //[Key]
        public Guid ItemId { get; set; }

        [Required]
        public int Quantity { get; set; }

        public Guid UnitID { get; set; }

        public int QuantityInSmallestUnit { get; set; }

        public string UnitName { get; set; }

        // Foreign Key Tables
        public TransferRecord TransfarRecord { get; set; }

        public Item Item { get; set; }

        public Unit Unit { get; set; }

        // Constructor
        public TransferItem()
        {
            Quantity = 0;
        }
    }
}
