﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hotel.Models
{
    public class Service
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public double Charges { get; set; }

        public bool IsDelete { get; set; }

        public DateTime CreatedOn { get; set; }

        public DateTime ModifiedOn { get; set; }

        public string CreatedBy { get; set; }

        public string ModifiedBy { get; set; }

        public Service()
        {

        }
    }
}
