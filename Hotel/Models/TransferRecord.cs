﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Hotel.Models
{
    public class TransferRecord
    {
        public Guid ID { get; set; }

        [Required]
        public Guid MainStoreID { get; set; }

        [Required]
        public Guid RoomId { get; set; }

        [Required]
        public DateTime TransferDate { get; set; }

        public DateTime? CreatedOn { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public string ModifiedBy { get; set; }

        // Foreign Key Tables
        public MainStore MainStore { get; set; }

        public Room Room { get; set; }

        [Required]
        public ICollection<TransferItem> TransferItems { get; set; }

        // Constructor
        public TransferRecord()
        {
            ModifiedOn = DateTime.UtcNow.AddHours(6).AddMinutes(30);
        }
    }
}
