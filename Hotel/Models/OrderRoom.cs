﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hotel.Models
{
    public class OrderRoom
    {
        public Guid Id { get; set; }

        public Guid OrderId { get; set; }

        public Guid RoomId { get; set; }

        public double RoomCharges { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public int ExtraBedQty { get; set; }

        public OrderRoomStatus Status { get; set; }

        public bool IsDebt { get; set; }

        public DateTime StatusModifiedDate { get; set; }

        //foreignkey table
        public Order Order { get; set; }

        public Room Room { get; set; }

        public ICollection<OrderRoomItem> OrderRoomItems { get; set; }

        public ICollection<OrderRoomService> OrderRoomServices { get; set; }

        public enum OrderRoomStatus
        {
            Requested = 1,
            Reserved = 2,
            CheckedIn = 3,
            CheckedOut = 4,
            Canceled = 5
        }
    }
}
