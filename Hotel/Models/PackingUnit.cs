﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Hotel.Models
{
    public class PackingUnit
    {
        public Guid ID { get; set; }

        [Required]
        public Guid ItemId { get; set; }

        [Required]
        public Guid UnitID { get; set; }

        [Required]
        public int QuantityInParent { get; set; } // This shows how many unit in its parent container.

        [Required]
        public decimal PurchasedAmount { get; set; }

        [Required]
        public decimal SaleAmount { get; set; }

        [Required]
        public int UnitOrder { get; set; }

        // Foreign Key Tables
        public Item Item { get; set; }

        public Unit Unit { get; set; }

        // Constructor
        public PackingUnit()
        {
            QuantityInParent = 1;

            PurchasedAmount = 0;

            SaleAmount = 0;
        }
    }
}
