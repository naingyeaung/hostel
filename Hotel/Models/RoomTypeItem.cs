﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hotel.Models
{
    public class RoomTypeItem
    {
        public Guid Id { get; set; }

        public Guid RoomTypeId { get; set; }

        public Guid ItemId { get; set; }

        public Guid UnitID { get; set; }

        public double UnitPrice { get; set; }

        public int Quantity { get; set; }

        public int QuantityInSmallestUnit { get; set; }

        public string UnitName { get; set; }

        //foreignkey table
        public RoomType RoomType { get; set; }

        public Item Item { get; set; }

        public Unit Unit { get; set; }

        public RoomTypeItem()
        {

        }
    }
}
