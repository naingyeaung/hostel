﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hotel.Models
{
    public class Order
    {
        public Guid Id { get; set; }

        public int VoucherNo { get; set; }

        public string CustomerName { get; set; }

        public string CustomerNRC { get; set; }

        public string CustomerPhone { get; set; }

        public string CustomerEmail { get; set; }

        public double TotalCharges { get; set; }

        public DateTime CreatedOn { get; set; }

        //foreignkey table
        public ICollection<OrderRoom> OrderRooms { get; set; }

        public Order()
        {

        }
    }
}
