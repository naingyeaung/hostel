﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Hotel.Models
{
    public class Counter
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public CounterType Type { get; set; }

        public int Count { get; set; }

        public enum CounterType
        {
            ItemCounter = 1,
            VoucherCount = 2
        }
    }
}
