﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Hotel.Models
{
    public class RoomItem
    {
        public Guid ID { get; set; }

        [Required]
        public Guid RoomId { get; set; }

        [Required]
        public Guid ItemId { get; set; }

        [Required]
        public int Quantity { get; set; }

        // Foreign Key Tables
        public Room Room { get; set; }

        public Item Item { get; set; }

        public RoomItem()
        {

        }
    }
}
