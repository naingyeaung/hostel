﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hotel.Models
{
    public class OrderRoomItem
    {
        public Guid Id { get; set; }

        public Guid OrderRoomId { get; set; }

        public Guid ItemId { get; set; }

        public Guid UnitID { get; set; }

        public double UnitPrice { get; set; }

        public int Quantity { get; set; }

        public int QuantityInSmallestUnit { get; set; }

        public string UnitName { get; set; }

        public bool IsFOC { get; set; }

        //foreignkey table
        public OrderRoom OrderRoom { get; set; }

        public Item Item { get; set; }

        public Unit Unit { get; set; }

        public OrderRoomItem()
        {

        }
    }
}
