﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Hotel.Models
{
    public class MainStoreStockRecord
    {
        public Guid Id { get; set; }

        [Required]
        public Guid MainStoreId { get; set; }

        [Required]
        public Guid ItemId { get; set; }

        [Required]
        public DateTime StockDate { get; set; }

        public DateTime? ExpiredDate { get; set; }

        [Required]
        public int Quantity { get; set; }

        public Guid? UnitID { get; set; }

        public int QuantityInSmallestUnit { get; set; }

        public string UnitName { get; set; }

        public bool IsOutOfStock { get; set; }

        public DateTime? CreatedOn { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public string ModifiedBy { get; set; }

        // Foreign Key Tables
        public MainStore MainStore { get; set; }

        public Item Item { get; set; }

        public Unit Unit { get; set; }

        // Constructor
        public MainStoreStockRecord()
        {
            ModifiedOn = DateTime.UtcNow.AddHours(6).AddMinutes(30);

            IsOutOfStock = false;
        }
    }
}
