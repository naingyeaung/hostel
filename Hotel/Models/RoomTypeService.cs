﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hotel.Models
{
    public class RoomTypeService
    {
        public Guid Id { get; set; }

        public Guid RoomTypeId { get; set; }

        public Guid ServiceId { get; set; }

        //foreignkey table 
        public RoomType RoomType { get; set; }

        public Service Service { get; set; }

        public RoomTypeService()
        {

        }
    }
}
