﻿using Hotel.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hotel.Entities
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            foreach (var relationship in builder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }
            base.OnModelCreating(builder);
        }

        public DbSet<Item> Items { get; set; }

        public DbSet<Order> Orders { get; set; }

        public DbSet<OrderRoom> OrderRooms { get; set; }

        public DbSet<OrderRoomItem> OrderRoomItems { get; set; }

        public DbSet<OrderRoomService> OrderRoomServices { get; set; }

        public DbSet<Room> Rooms { get; set; }

        public DbSet<RoomPhoto> RoomPhotos { get; set; }

        public DbSet<RoomType> RoomTypes { get; set; }

        public DbSet<RoomTypeItem> RoomTypeItems { get; set; }

        public DbSet<RoomTypeService> RoomTypeServices { get; set; }

        public DbSet<RoomItem> RoomItems { get; set; }

        public DbSet<Service> Services { get; set; }

        public DbSet<Counter> Counters { get; set; }

        public DbSet<MainStore> MainStores { get; set; }

        public DbSet<MainStoreItem> MainStoreItems { get; set; }

        public DbSet<MainStoreStockRecord> MainStoreStockRecords { get; set; }

        public DbSet<PackingUnit> PackingUnits { get; set; }

        public DbSet<TransferItem> TransferItems { get; set; }

        public DbSet<TransferRecord> TransferRecords { get; set; }

        public DbSet<Unit> Units { get; set; }
    }
}
