import React from "react";
import PropTypes from "prop-types";
// @material-ui/core components
import Table from "components/Table/Table.js";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import Button from "components/CustomButtons/Button.js";
import { NavLink } from "react-router-dom";
import { Switch, Route, Redirect } from "react-router-dom";
import Create from '../Item/create.js';
import { getData } from "components/fetch.js";
import { Grid, ButtonBase, Badge } from "@material-ui/core";


export default class RoomIndex extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            rooms: []
        }
    }

    async componentDidMount() {
        var response = await getData('/api/room/index')
        if (response.ok) {
            this.state.rooms = response.data;
            this.setState({ rooms: this.state.rooms });
            console.log(this.state.rooms)
        } else {
            console.log(response.data);
        }
    }

    render() {
        return (
            <>
                <NavLink to='/admin/room/create'>
                    <Button color="info">Create New</Button>
                </NavLink>
                <br></br>
                <br></br>
                <h4>Ground Floor</h4>
                <Grid container direction='row'>
                    {
                        this.state.rooms.map((item, index) =>

                            <Grid item md={4} style={{height:400}}>

                                <div style={{ width: 350, height: 150, backgroundSize: 'cover', backgroundImage: 'url(/images/rooms/' + item.room.roomPhotos[0].photoName + ')' }}>
                                    {/* <div style={{ height: 10 }}></div>
                                <GridContainer>
                                    <GridItem md={6}>
                                        <h3 style={{ color: 'white', fontWeight: 'bold',alignSelf:'center' }}>{item.room.roomType.name} Room</h3>
                                    </GridItem>
                                    <GridItem md={6}>
                                        <h4 style={{color:'white',fontWeight:'bold'}}>RoomNo {item.room.roomNo}</h4>
                                    </GridItem>
                                </GridContainer> */}

                                </div>
                                <div style={{ width: 350, backgroundColor: '#DCDCDC' }}>
                                    {/* <label>Current Status : {item.currentStatus}</label>
                                <br></br>
                                <label>Book Requests : {item.requestStatus}</label>
                                <br></br>
                                <label>Reservations : {item.reservationStatus}</label>
                                <Button color='info'>Rserve</Button> */}
                                    <Grid container justify="center">
                                        <Grid item md={12}><p style={{ textAlign: 'center' }}>Room - {item.room.roomNo}</p></Grid>
                                    </Grid>
                                    <Grid container justify='center' direction='row'>
                                        <Grid item md={6} justify='center' alignItems='center'>
                                            <p style={{ fontWeight: 'bold', textAlign: 'center' }}>{item.room.roomType.name}</p>
                                        </Grid>
                                        <Grid item md={6}>
                                            <p style={{ textAlign: 'center' }}>{item.room.roomType.roomPrice} ks</p>
                                        </Grid>
                                    </Grid>
                                    <Grid container justify='center' direction='row' alignItems="center">
                                        <Grid item md={3} justify='center' alignItems="center" alignContent='center'>
                                            <p style={{ textAlign: 'center' }}>
                                                <i class="material-icons md-48" style={{ alignSelf: 'center', color: item.checkin.length > 0 ? 'red' : 'green' }}>{item.checkin.length>0?'lock':'lock_open'}</i>
                                            </p>
                                        </Grid>
                                        <Grid item md={3} justify='center' alignItems='center' alignContent='center'>
                                            <p style={{ textAlign: 'center' }}>
                                                <Badge badgeContent={item.requests.length} color="secondary">
                                                    <i class="material-icons md-48" style={{ textAlign: 'center' }}>notifications_active</i>
                                                </Badge>
                                            </p>
                                        </Grid>
                                        <Grid item md={3}>
                                            <p style={{ textAlign: 'center' }}>
                                                <Badge badgeContent={item.reservations.length} color="secondary">
                                                    <i class="material-icons md-48" style={{ textAlign: 'center' }}>calendar_today</i>
                                                </Badge>
                                            </p>
                                        </Grid>
                                    </Grid>
                                    <Grid container direction='row' style={{ height: 40 }}>
                                        <Grid item md={8} style={{ backgroundColor: 'gray' }}>
                                            <ButtonBase style={{ width: '100%', height: '100%' }}>
                                                <p style={{ textAlign: 'center', color: 'white', fontWeight: 'bold' }}>View Reservations</p>
                                            </ButtonBase>
                                        </Grid>
                                        <Grid item md={4} style={{ backgroundColor: '#00acc1' }}>
                                            <ButtonBase style={{ width: '100%', height: '100%' }}>
                                                <p style={{ textAlign: 'center', color: 'white', fontWeight: 'bold' }}>Book</p>
                                            </ButtonBase>
                                        </Grid>
                                    </Grid>

                                </div>
                            </Grid>
                        )
                    }
                </Grid>

            </>
        )
    }

}
