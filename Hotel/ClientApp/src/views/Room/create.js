import React from "react";
import { MenuItem, FormControl, Grid, Input } from "@material-ui/core";
import Button from "components/CustomButtons/Button.js";
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import Table from "components/Table/Table.js";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import { getData } from "components/fetch";
import { postData } from "components/fetch";
import Select from 'react-select'
import photoUploadIcon from "assets/img/addPhotoIcon.png";

export default class RoomCreate extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            roomTypes: [],
            photos: [],
            photoBytes: [],
            roomNo : '',
            floor:{},
            roomType :{}
        }
    }

    async componentDidMount() {
        var response = await getData('/api/roomtype/index');
        if (response.ok) {
            this.state.roomTypes = response.data;
            this.setState({ roomTypes: this.state.roomTypes });
        } else {
            console.log(response.data)
        }
    }

    addPhoto(event) {

        this.readURL(event.target.files[0]);

        var photo = { link: event.target.files[0] };
        this.state.photos.push(photo);
        this.setState({ photos: this.state.photos });
    }

    readURL(input) {
        var parent = this;

        var reader = new FileReader();
        reader.readAsDataURL(input);
        reader.onload = function (e) {

            var photoState = { Link: e.target.result };
            parent.state.photoBytes.push(photoState);
            parent.setState({ photoBytes: parent.state.photoBytes })

        }
    }

    async save() {
        var photos = [];
        this.state.photoBytes.map((byte,index)=>{
            var photo = {PhotoName:byte.Link,PhotoOrder:index};
            photos.push(photo);
        })

        var model = {
            RoomNo : this.state.roomNo,
            Floor : this.state.floor.value,
            RoomTypeId : this.state.roomType.value,
            RoomPhotos : photos
        };

        var response = await postData('/api/room/create',model);
        if(response.ok){
            window.location.href="/admin/room/index"
        }else{
            console.log(response.data);
        }

        console.log(model);
    }

    render() {
        const floors = [{ value: 1, label: "Ground" }, { value: 2, label: "First" }, { value: 3, label: "Second" }]
        return (
            <>
                <GridContainer style={{ width: 1000 }}>
                    <GridItem md={12}>
                        <label>Room Photos</label>
                        <br></br>
                        <GridContainer>
                            {
                                this.state.photos.map((photo, i) =>
                                    <GridItem>
                                        <label className="upload-label-default text-center">
                                            <img src={URL.createObjectURL(photo.link)} width="100" height="100" />
                                        </label>
                                    </GridItem>
                                )
                            }
                            <GridItem>
                                <label className="upload-label text-center">
                                    <img src={photoUploadIcon} width="100" height="100" />
                                    <input type="file" id="photo" name="photo" className="form-control" onChange={(e) => this.addPhoto(e)} style={{ display: "none" }} multiple />
                                </label>
                            </GridItem>
                        </GridContainer>
                    </GridItem>
                </GridContainer>
                <GridContainer style={{ width: 1000 }}>
                    <GridItem md={6}>
                        <GridContainer>
                            <GridItem md={4}>
                                <label>RoomNo</label>
                            </GridItem>
                            <GridItem md={8}>
                                <Input label="Name" type="text" style={{ width: '100%' }} value={this.state.roomNo} onChange={(event) => this.setState({ roomNo: event.target.value })}></Input>
                            </GridItem>
                        </GridContainer>

                    </GridItem>

                </GridContainer>
                <br></br>
                <GridContainer style={{ width: 1000 }}>
                    <GridItem md={6}>
                        <GridContainer>
                            <GridItem md={4}>
                                <label>Floor</label>
                            </GridItem>
                            <GridItem md={8}>
                                <Select options={floors} value={this.state.floor} onChange={(value)=>this.setState({floor:value})}></Select>
                            </GridItem>
                        </GridContainer>
                    </GridItem>
                </GridContainer>
                <br></br>
                <GridContainer style={{ width: 1000 }}>
                    <GridItem md={6}>
                        <GridContainer>
                            <GridItem md={4}>
                                <label>RoomType</label>
                            </GridItem>
                            <GridItem md={8}>
                                <Select onChange={(value)=>this.setState({roomType:value})} value={this.state.roomType} options={this.state.roomTypes.map((type, index) => { return { value: type.id, label: type.name } })}></Select>
                            </GridItem>
                        </GridContainer>
                    </GridItem>
                </GridContainer>
                <br></br>
                <Button color="info" onClick={() => this.save()}>Create</Button>
            </>
        )
    }
}