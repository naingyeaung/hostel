import React from "react";
import { MenuItem, FormControl, Grid, Input } from "@material-ui/core";
import Button from "components/CustomButtons/Button.js";
import Select from '@material-ui/core/Select';
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import Table from "components/Table/Table.js";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import { getData } from "components/fetch";
import { postData } from "components/fetch";

export default class ServiceCreate extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name : '',
            charges : ''
        }
    }

    async save(){
        var model = {
            Name : this.state.name,
            Charges : this.state.charges
        }

        var response = await postData('/api/service/create', model);
        if(response.ok){
            window.location.href="/admin/service/index";
        }else{
            console.log(response.data);
        }
    }

    render() {
        return (
            <>
                <GridContainer style={{ width: 1000 }}>
                    <GridItem md={6}>
                        <GridContainer>
                            <GridItem md={2}>
                                <label>Name</label>
                            </GridItem>
                            <GridItem md={8}>
                                <Input label="Name" type="text" style={{ width: '100%' }} value={this.state.name} onChange={(event) => this.setState({ name: event.target.value })}></Input>
                            </GridItem>
                        </GridContainer>

                    </GridItem>
                    <GridItem md={6}>
                        <GridContainer>
                            <GridItem md={2}>
                                <label>Charges</label>
                            </GridItem>
                            <GridItem md={8}>
                                <Input label="Charges" type="number" style={{ width: '100%' }} value={this.state.charges} onChange={(event) => this.setState({ charges: event.target.value })}></Input>
                            </GridItem>
                        </GridContainer>

                    </GridItem>
                </GridContainer>
                <br></br>
                <Button color="info" onClick={()=>this.save()}>Create</Button>
            </>
        )
    }
}