import React from "react";
import PropTypes from "prop-types";
// @material-ui/core components
import Table from "components/Table/Table.js";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import Button from "components/CustomButtons/Button.js";
import { NavLink } from "react-router-dom";
import { Switch, Route, Redirect } from "react-router-dom";
import Create from '../Item/create.js';
import { getData } from "components/fetch.js";

export default class ServiceIndex extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            services : []
        }
    }

    async componentDidMount() {
        var response = await getData('/api/service/index')
        if (response.ok) {
            this.state.services = response.data;
            this.setState({ services: this.state.services });
        } else {
            console.log(response.data);
        }
    }

    render() {
        return (
            <>
                <NavLink to='/admin/service/create'>
                    <Button color="info">Create New</Button>
                </NavLink>
                <Table
                    tableHeaderColor="info"
                    tableHead={["Name", "Charges", ""]}>
                    {
                        this.state.services.map((item, index) =>
                            <TableRow>
                                <TableCell>{item.name}</TableCell>
                                <TableCell>{item.charges}</TableCell>
                                <TableCell>
                                    <Button color="info">View</Button>
                                </TableCell>
                            </TableRow>
                        )
                    }

                </Table>
            </>
        )
    }

}