import React from "react";
import { MenuItem, FormControl, Grid, Input } from "@material-ui/core";
import Button from "components/CustomButtons/Button.js";
import Select from '@material-ui/core/Select';
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import Table from "components/Table/Table.js";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";

export default class PackingUnits extends React.Component {
    constructor(props) {
        super(props);
        // packingunits.sort(this.compare);
        this.state = {
            text: props.value,
            allunits: [],
            allpackingunits: [],
            virtualpackingunits: [],
            UnitPrice: 0,
            PercentageForSale: 0,
            action: "create",
            permitted: true
        };

        this.changeQty = this.changeQty.bind(this);
        this.handleInput = this.handleInput.bind(this);
        this.deleteUnit = this.deleteUnit.bind(this);
    }

    reloadPackingUnits() {
        var puorder = this.state.virtualpackingunits.length > 0 ? Math.max.apply(Math, this.state.virtualpackingunits.map(function (o) { return o.UnitOrder; })) : 0;
        console.log(puorder);
        console.log(this.state.allunits);
        var units = this.state.allunits.filter(e => e.UnitOrder >= puorder + 1);
        console.log(units);
        this.setState({ allpackingunits: units });
    }

    compare(a, b) {
        if (a.UnitOrder < b.UnitOrder)
            return -1;
        if (a.UnitOrder > b.UnitOrder)
            return 1;
        if (a.UnitOrder === b.UnitOrder) {
            if (a.Description < b.Description)
                return -1;
            if (a.Description > b.Description)
                return 1;
        }
        return 0;
    }

    changeQty(e) {
        var id = e.target.id;
        var value = e.target.value;
        for (var i = this.state.virtualpackingunits.length - 1; i >= 0; i--) {
            if (this.state.action.toLocaleLowerCase() == "create") {
                if ("QuantityInParent" + this.state.virtualpackingunits[i].ID == id) {
                    this.state.virtualpackingunits[i].QuantityInParent = value;
                    this.setState({ virtualpackingunits: this.state.virtualpackingunits });
                    this.calculatePrice();
                    return false;
                }
            }
            if ((this.state.action.toLocaleLowerCase() == "create" || this.state.action.toLocaleLowerCase() == "edit") && this.state.permitted) {
                if ("SaleAmount" + this.state.virtualpackingunits[i].ID == id) {
                    this.state.virtualpackingunits[i].SaleAmount = value;
                    this.setState({ virtualpackingunits: this.state.virtualpackingunits });
                    return false;
                }
            }
        }
    }

    async handleInput(e) {
        var value = e.target.value;
        await this.setState({ [e.target.name]: e.target.value });
        this.calculatePrice();
    }

    addUnit(count) {
        if (this.state.action.toLocaleLowerCase() == "create") {
            var id = document.getElementById("unitToAddID").value;
            var unit = null;
            for (var i = this.state.allpackingunits.length - 1; i >= 0; i--) {
                if (this.state.allpackingunits[i].ID == id) {
                    unit = this.state.allpackingunits[i];
                    // this.state.allpackingunits.splice(i, 1);
                }
            }
            // this.setState({ allpackingunits: this.state.allpackingunits });
            if (unit !== null) {
                this.state.virtualpackingunits.push(unit);
                this.reloadPackingUnits();
                this.calculatePrice();
            }
        }
    }

    deleteUnit(id) {
        if (this.state.action.toLocaleLowerCase() == "create") {
            var unit = null;
            for (var i = this.state.virtualpackingunits.length - 1; i >= 0; i--) {
                if (this.state.virtualpackingunits[i].ID === id) {
                    this.state.virtualpackingunits.splice(i, 1);
                }
            }
            this.reloadPackingUnits();
            this.calculatePrice();
        }
    }

    calculatePrice() {
        var qty = 1;
        var unitprice = this.state.UnitPrice;
        var percentage = this.state.PercentageForSale;
        if (this.state.virtualpackingunits.length > 0) {
            this.state.virtualpackingunits.map(function (o) {
                qty *= o.QuantityInParent;
                var purchaseamount = unitprice / qty;
                var saleamount = purchaseamount + (purchaseamount * percentage) / 100;
                o.PurchasedAmount = Math.round(purchaseamount);
                o.SaleAmount = Math.round(saleamount);
            });
            this.setState({ virtualpackingunits: this.state.virtualpackingunits });
        }
    }

    render() {
        return (
            <div>
                <GridContainer style={{ width: 1200 }}>
                    <GridItem md={1}>
                        <label>Packing Unit</label>
                    </GridItem>
                    <GridItem md={2}>
                        <Select style={{ width: '100%' }} className={(this.state.action.toLowerCase() != "create" ? "d-none " : "") + "form-control react-input-1 col-4 pull-left"} id="unitToAddID">
                            {
                                this.state.allpackingunits.map(item =>
                                    <MenuItem key={item.ID} value={item.ID}>
                                        {item.Description}
                                    </MenuItem>
                                )
                            }
                        </Select>
                    </GridItem>
                    <GridItem md={1}>
                        <Button onClick={() => this.addUnit} color="white">+</Button>
                    </GridItem>

                    {/* <a onClick={() => this.addUnit()} className={(this.state.action.toLowerCase() != "create" ? "d-none " : "") + "form-control nav-link col-1 pull-right px-3 mx-1"}><i className="nc-icon nc-simple-add"></i></a> */}
                    {
                        this.state.permitted ? (
                            <>
                                <GridItem md={1}>
                                    <label>Unit Price : </label>
                                </GridItem>
                                <GridItem md={2}>
                                    <Input type="number" className={"form-control col-5 mr-1"} min="0" id="UnitPrice" name="UnitPrice" value={this.state.UnitPrice} onChange={this.handleInput} placeholder="Unit Price" />
                                </GridItem>
                                <GridItem md={2}>
                                    <Input type="number" className={"form-control col"} style={{ maxWidth: 100 }} min="0" max="100" id="PercentageForSale" name="PercentageForSale" value={this.state.PercentageForSale} onChange={this.handleInput} placeholder="Sale %" />
                                </GridItem>
                                <GridItem md={1}>
                                    <label>%</label>
                                </GridItem>
                            </>
                        ) : (
                                <div className="row col">
                                    <span className={"mx-2 py-2"}>Unit Price : </span>
                                    <input type="number" className={"form-control col-5 mr-1"} min="0" id="UnitPrice" name="UnitPrice" value={this.state.UnitPrice} onChange={this.handleInput} placeholder="Unit Price" />
                                </div>
                            )
                    }
                </GridContainer>
                <div className={"col-12 my-2 px-1" + (this.state.virtualpackingunits.length == 0 ? " d-none" : "")}>
                    {
                        this.state.permitted ? (
                            <Table
                                tableHeaderColor="info"
                                tableHead={["Unit", "Qty", "Purchase", "Sale", ""]}>
                                {
                                    this.state.virtualpackingunits.map((item, i) =>
                                        <TableRow id={item.ID} key={i}>
                                            <input type="hidden" name={"PackingUnits[" + i + "].UnitID"} value={item.ID} />
                                            <input type="hidden" name={"PackingUnits[" + i + "].QuantityInParent"} value={item.QuantityInParent} />
                                            <input type="hidden" name={"PackingUnits[" + i + "].UnitOrder"} value={i + 1} />
                                            <TableCell>{item.Description}</TableCell>
                                            <TableCell>
                                                <Input type="number" id={"QuantityInParent" + item.ID} onChange={this.changeQty} value={item.QuantityInParent} className="form-control" disabled={(i == 0 || this.state.action.toLowerCase() != "create") ? "disabled" : ""} />
                                            </TableCell>
                                            {
                                                this.state.permitted ? (
                                                    <>
                                                        <TableCell>
                                                            <span className="">{item.PurchasedAmount}</span>
                                                            <Input type="number" name={"PackingUnits[" + i + "].PurchasedAmount"} value={item.PurchasedAmount} onChange={() => { }} className={(!this.state.permitted ? "d-none " : "") + "form-control d-none"} />
                                                        </TableCell>
                                                        <TableCell>
                                                            <input type="number" id={"SaleAmount" + item.ID} name={"PackingUnits[" + i + "].SaleAmount"} onChange={this.changeQty} value={item.SaleAmount} className={(!this.state.permitted ? "d-none " : "") + "form-control"} />
                                                        </TableCell>
                                                    </>
                                                ) : (
                                                        <div className="row col">
                                                            <div className={(!this.state.permitted ? "d-none " : "col-5 py-2")}>
                                                                <span className="">{item.PurchasedAmount}</span>
                                                                <input type="decimal" name={"PackingUnits[" + i + "].PurchasedAmount"} value={item.PurchasedAmount} onChange={() => { }} className={(!this.state.permitted ? "d-none " : "") + "form-control d-none"} />
                                                            </div>
                                                        </div>
                                                    )
                                            }
                                            {
                                                this.state.action.toLowerCase() == "create" ? (
                                                    <TableCell>
                                                        <i onClick={() => this.deleteUnit(item.ID)} className="nc-icon nc-simple-remove pull-right"></i>
                                                    </TableCell>
                                                ) : (<TableCell></TableCell>)
                                            }
                                        </TableRow>
                                    )
                                }
                            </Table>
                            // <div className="row col">
                            //     <label className="col-5">Unit</label>
                            //     <label className="col-2">Qty</label>
                            //     <label className="col-2">Purchase</label>
                            //     <label className="col-2">Sale</label>
                            //     <label className="col-1"></label>
                            // </div>
                        ) : (<div>
                            <label className="col-5">Unit</label>
                            <label className="col-2">Qty</label>
                        </div>)
                    }
                </div>
                {/* <span>{this.state.UnitPrice}<br />{this.state.PercentageForSale} %<br />{(this.state.UnitPrice * this.state.PercentageForSale) / 100} kyats<br />{JSON.stringify(this.state.virtualpackingunits)}</span> */}
            </div>
        );
    }
}

