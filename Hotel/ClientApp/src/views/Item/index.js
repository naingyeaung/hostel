import React from "react";
import PropTypes from "prop-types";
// @material-ui/core components
import Table from "components/Table/Table.js";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import Button from "components/CustomButtons/Button.js";
import { NavLink } from "react-router-dom";
import { Switch, Route, Redirect } from "react-router-dom";
import Create from '../Item/create.js';
import { getData } from "components/fetch.js";

export default class extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            items: []
        }
    }

    async componentDidMount() {
        var response = await getData('/api/item/index')
        if (response.ok) {
            this.state.items = response.data;
            this.setState({ items: this.state.items });
        } else {
            console.log(response.data);
        }
    }

    render() {
        return (
            <>
                <NavLink to='/admin/item/create/0'>
                    <Button color="info">Create New</Button>
                </NavLink>
                <Table
                    tableHeaderColor="info"
                    tableHead={["Name", "Code", "Price", "SalePercentage", ""]}>
                    {
                        this.state.items.map((item, index) =>
                            <TableRow>
                                <TableCell>{item.name}</TableCell>
                                <TableCell>{item.code}</TableCell>
                                <TableCell>{item.unitPrice}</TableCell>
                                <TableCell>{item.percentageForSale}</TableCell>
                                <TableCell>
                                    <NavLink to={'/admin/item/create/'+item.id}>
                                        <Button color="info">View</Button>
                                    </NavLink>
                                </TableCell>
                            </TableRow>
                        )
                    }

                </Table>
            </>
        )
    }

}