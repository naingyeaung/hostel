import React from "react";
import { MenuItem, FormControl, Grid, Input, ButtonBase } from "@material-ui/core";
import Button from "components/CustomButtons/Button.js";
import Select from '@material-ui/core/Select';
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import Table from "components/Table/Table.js";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import { getData } from "components/fetch";
import { postData } from "components/fetch";

export default class ItemCreate extends React.Component {
    constructor(props) {
        super(props);
        // packingunits.sort(this.compare);
        let { id } = this.props.match.params;
        this.state = {
            productId: id,
            text: props.value,
            allunits: [],
            allpackingunits: [],
            virtualpackingunits: [],
            UnitPrice: 0,
            PercentageForSale: 0,
            action: "create",
            permitted: true,
            itemID: '',
            name: ''
        };

        this.changeQty = this.changeQty.bind(this);
        this.handleInput = this.handleInput.bind(this);
        this.deleteUnit = this.deleteUnit.bind(this);
        this.loadItem = this.loadItem.bind(this);
    }

    populatePackingUnits = async () => {
        var response = await getData("/api/unit/index");
        if (response.ok) {
            // console.log(response.data)
            response.data.map(async element => {
                var obj = { ID: element.id, Description: element.description, UnitOrder: element.unitOrder, ActualOrder: null, QuantityInParent: 1, PurchasedAmount: 0, SaleAmount: 0 };
                this.state.allunits.push(obj);
                this.state.allpackingunits.push(obj);
                this.setState({ allunits: this.state.allunits });
                this.setState({ allpackingunits: this.state.allpackingunits });
            });
        } else {
            console.log(response.data);
        }
    }

    async componentDidMount() {
        await this.populatePackingUnits();
        if (this.state.productId != 0) {
            await this.loadItem();
        }
    }


    async loadItem() {
        var response = await getData('/api/item/detail?id=' + this.state.productId);
        if (response.ok) {
            console.log(response.data)
            var item = response.data
            this.setState({ name: item.name });
            this.setState({ UnitPrice: item.unitPrice });
            this.setState({ PercentageForSale: item.percentageForSale });
            item.packingUnits.map((element, index) => {
                var obj = { ID: element.id, Description: this.state.allpackingunits.filter(x => x.ID == element.unitID).length > 0 ? this.state.allpackingunits.filter(x => x.ID == element.unitID)[0].Description : '', UnitOrder: element.unitOrder, ActualOrder: null, QuantityInParent: element.quantityInParent, PurchasedAmount: element.purchasedAmount, SaleAmount: element.saleAmount };
                this.state.virtualpackingunits.push(obj);
                this.setState({ virtualpackingunits: this.state.virtualpackingunits });
                console.log(this.state.virtualpackingunits);
            });
        } else {
            console.log(response.data);
        }
    }

    reloadPackingUnits() {
        var puorder = this.state.virtualpackingunits.length > 0 ? Math.max.apply(Math, this.state.virtualpackingunits.map(function (o) { return o.UnitOrder; })) : 0;
        console.log(puorder);
        console.log(this.state.allunits);
        var units = this.state.allunits.filter(e => e.UnitOrder >= puorder + 1);
        console.log(units);
        this.setState({ allpackingunits: units });
    }

    compare(a, b) {
        if (a.UnitOrder < b.UnitOrder)
            return -1;
        if (a.UnitOrder > b.UnitOrder)
            return 1;
        if (a.UnitOrder === b.UnitOrder) {
            if (a.Description < b.Description)
                return -1;
            if (a.Description > b.Description)
                return 1;
        }
        return 0;
    }

    changeQty(e) {
        var id = e.target.id;
        var value = e.target.value;
        for (var i = this.state.virtualpackingunits.length - 1; i >= 0; i--) {
            if (this.state.action.toLocaleLowerCase() == "create") {
                if ("QuantityInParent" + this.state.virtualpackingunits[i].ID == id) {
                    this.state.virtualpackingunits[i].QuantityInParent = value;
                    this.setState({ virtualpackingunits: this.state.virtualpackingunits });
                    this.calculatePrice();
                    return false;
                }
            }
            if ((this.state.action.toLocaleLowerCase() == "create" || this.state.action.toLocaleLowerCase() == "edit") && this.state.permitted) {
                if ("SaleAmount" + this.state.virtualpackingunits[i].ID == id) {
                    this.state.virtualpackingunits[i].SaleAmount = value;
                    this.setState({ virtualpackingunits: this.state.virtualpackingunits });
                    return false;
                }
            }
        }
    }

    async handleInput(e) {
        var value = e.target.value;
        await this.setState({ [e.target.name]: e.target.value });
        this.calculatePrice();
    }

    addUnit(count) {
        if (this.state.action.toLocaleLowerCase() == "create") {
            var id = document.getElementById("unitToAddID").value;
            var unit = null;
            for (var i = this.state.allpackingunits.length - 1; i >= 0; i--) {
                if (this.state.allpackingunits[i].ID == id) {
                    unit = this.state.allpackingunits[i];
                    unit.UnitID = id;
                    unit.ID = undefined;
                    // this.state.allpackingunits.splice(i, 1);
                }
            }
            // this.setState({ allpackingunits: this.state.allpackingunits });
            if (unit !== null) {
                this.state.virtualpackingunits.push(unit);
                this.reloadPackingUnits();
                this.calculatePrice();
            }
        }
    }

    deleteUnit(id) {
        if (this.state.action.toLocaleLowerCase() == "create") {
            var unit = null;
            for (var i = this.state.virtualpackingunits.length - 1; i >= 0; i--) {
                if (this.state.virtualpackingunits[i].ID === id) {
                    this.state.virtualpackingunits.splice(i, 1);
                }
            }
            this.reloadPackingUnits();
            this.calculatePrice();
        }
    }

    calculatePrice() {
        var qty = 1;
        var unitprice = this.state.UnitPrice;
        var percentage = this.state.PercentageForSale;
        if (this.state.virtualpackingunits.length > 0) {
            this.state.virtualpackingunits.map(function (o) {
                qty *= o.QuantityInParent;
                var purchaseamount = unitprice / qty;
                var saleamount = purchaseamount + (purchaseamount * percentage) / 100;
                o.PurchasedAmount = Math.round(purchaseamount);
                o.SaleAmount = Math.round(saleamount);
            });
            this.setState({ virtualpackingunits: this.state.virtualpackingunits });
        }
    }

    async save() {
        var model = {
            Name: this.state.name,
            UnitPrice: this.state.UnitPrice,
            PercentageForSale: this.state.PercentageForSale,
            PackingUnits: this.state.virtualpackingunits
        }
        console.log(model);
        var response = await postData("/api/item/create", model);
        if (response.ok) {
            window.location.href = "/admin/item/index";
        } else {
            console.log(response.data);
        }


    }

    render() {
        return (
            <div>
                <GridContainer style={{ width: 1000 }}>
                    <GridItem md={6}>
                        <GridContainer>
                            <GridItem md={2}>
                                <label>Name</label>
                            </GridItem>
                            <GridItem md={8}>
                                <Input label="Name" type="text" style={{ width: '100%' }} value={this.state.name} onChange={(event) => this.setState({ name: event.target.value })}></Input>
                            </GridItem>
                        </GridContainer>

                    </GridItem>
                </GridContainer>
                <br></br>
                {/* <GridContainer style={{ width: 1000 }}>
                    <GridItem md={6}>
                        <GridContainer>
                            <GridItem md={2}>
                                <label>Code</label>
                            </GridItem>
                            <GridItem md={8}>
                                <Input label="Name" type="text" style={{width:'100%'}}></Input>
                            </GridItem>
                        </GridContainer>

                    </GridItem>
                </GridContainer>
                <br></br> */}
                <br></br>
                <GridContainer style={{ width: 1200 }}>
                    <GridItem md={1}>
                        <label>Packing Unit</label>
                    </GridItem>
                    <GridItem md={2}>
                        <Select value={this.state.itemID} style={{ width: '100%' }} onChange={(event) => this.setState({ itemID: event.target.value })} className={(this.state.action.toLowerCase() != "create" ? "d-none " : "") + "form-control react-input-1 col-4 pull-left"} id="unitToAddID">
                            {
                                this.state.allpackingunits.map(item =>
                                    <MenuItem key={item.ID} value={item.ID}>
                                        {item.Description}
                                    </MenuItem>
                                )
                            }
                        </Select>
                    </GridItem>
                    <GridItem md={1}>
                        <Button onClick={() => this.addUnit()} color="white">+</Button>
                    </GridItem>

                    {/* <a onClick={() => this.addUnit()} className={(this.state.action.toLowerCase() != "create" ? "d-none " : "") + "form-control nav-link col-1 pull-right px-3 mx-1"}><i className="nc-icon nc-simple-add"></i></a> */}
                    {
                        this.state.permitted ? (
                            <>
                                <GridItem md={1}>
                                    <label>Unit Price : </label>
                                </GridItem>
                                <GridItem md={2}>
                                    <Input type="number" className={"form-control col-5 mr-1"} min="0" id="UnitPrice" name="UnitPrice" value={this.state.UnitPrice} onChange={this.handleInput} placeholder="Unit Price" />
                                </GridItem>
                                <GridItem md={2}>
                                    <Input type="number" className={"form-control col"} style={{ maxWidth: 100 }} min="0" max="100" id="PercentageForSale" name="PercentageForSale" value={this.state.PercentageForSale} onChange={this.handleInput} placeholder="Sale %" />
                                </GridItem>
                                <GridItem md={1}>
                                    <label>%</label>
                                </GridItem>
                            </>
                        ) : (
                                <div className="row col">
                                    <span className={"mx-2 py-2"}>Unit Price : </span>
                                    <input type="number" className={"form-control col-5 mr-1"} min="0" id="UnitPrice" name="UnitPrice" value={this.state.UnitPrice} onChange={this.handleInput} placeholder="Unit Price" />
                                </div>
                            )
                    }
                </GridContainer>
                <div className={"col-12 my-2 px-1" + (this.state.virtualpackingunits.length == 0 ? " d-none" : "")}>
                    {
                        this.state.permitted ? (
                            <Table
                                tableHeaderColor="info"
                                tableHead={["Unit", "Qty", "Purchase", "Sale", ""]}>
                                {
                                    this.state.virtualpackingunits.map((item, i) =>
                                        <TableRow id={item.ID} key={i}>
                                            <input type="hidden" name={"PackingUnits[" + i + "].UnitID"} value={item.UnitID} />
                                            <input type="hidden" name={"PackingUnits[" + i + "].QuantityInParent"} value={item.QuantityInParent} />
                                            <input type="hidden" name={"PackingUnits[" + i + "].UnitOrder"} value={i + 1} />
                                            <TableCell>{item.Description}</TableCell>
                                            <TableCell>
                                                <Input type="number" id={"QuantityInParent" + item.ID} onChange={this.changeQty} value={item.QuantityInParent} className="form-control" disabled={(i == 0 || this.state.action.toLowerCase() != "create") ? "disabled" : ""} />
                                            </TableCell>
                                            {
                                                this.state.permitted ? (
                                                    <>
                                                        <TableCell>
                                                            {/* <span className="">{item.PurchasedAmount}</span> */}
                                                            <Input type="number" name={"PackingUnits[" + i + "].PurchasedAmount"} value={item.PurchasedAmount} onChange={() => { }} className={(!this.state.permitted ? "d-none " : "") + "form-control d-none"} />
                                                        </TableCell>
                                                        <TableCell>
                                                            <Input type="number" id={"SaleAmount" + item.ID} name={"PackingUnits[" + i + "].SaleAmount"} onChange={this.changeQty} value={item.SaleAmount} className={(!this.state.permitted ? "d-none " : "") + "form-control"} />
                                                        </TableCell>
                                                    </>
                                                ) : (
                                                        <div className="row col">
                                                            <div className={(!this.state.permitted ? "d-none " : "col-5 py-2")}>
                                                                <span className="">{item.PurchasedAmount}</span>
                                                                <Input type="decimal" name={"PackingUnits[" + i + "].PurchasedAmount"} value={item.PurchasedAmount} onChange={() => { }} className={(!this.state.permitted ? "d-none " : "") + "form-control d-none"} />
                                                            </div>
                                                        </div>
                                                    )
                                            }
                                            {
                                                this.state.action.toLowerCase() == "create" ? (
                                                    <TableCell>
                                                        <ButtonBase color="white" onClick={() => this.deleteUnit(item.ID)}>
                                                            <i class="material-icons md-48">highlight_off</i>
                                                        </ButtonBase>
                                                        {/* <i onClick={() => this.deleteUnit(item.ID)} className="nc-icon nc-simple-remove pull-right">X</i> */}
                                                    </TableCell>
                                                ) : (<TableCell></TableCell>)
                                            }
                                        </TableRow>
                                    )
                                }
                            </Table>
                            // <div className="row col">
                            //     <label className="col-5">Unit</label>
                            //     <label className="col-2">Qty</label>
                            //     <label className="col-2">Purchase</label>
                            //     <label className="col-2">Sale</label>
                            //     <label className="col-1"></label>
                            // </div>
                        ) : (<div>
                            <label className="col-5">Unit</label>
                            <label className="col-2">Qty</label>
                        </div>)
                    }
                </div>
                <br></br>
                <GridContainer>
                    <Button color="info" onClick={() => this.save()}>Create</Button>
                </GridContainer>
                {/* <span>{this.state.UnitPrice}<br />{this.state.PercentageForSale} %<br />{(this.state.UnitPrice * this.state.PercentageForSale) / 100} kyats<br />{JSON.stringify(this.state.virtualpackingunits)}</span> */}
            </div>
        );
    }
}

