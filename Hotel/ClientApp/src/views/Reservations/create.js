import React from "react";
import { MenuItem, FormControl, Grid, Input, Table, TableHead, TableBody, ButtonBase, TextField } from "@material-ui/core";
import Select from 'react-select'
import Button from "components/CustomButtons/Button.js";
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import { getData } from "components/fetch";
import { postData } from "components/fetch";
import Checkbox from '@material-ui/core/Checkbox';
import { textChangeRangeIsUnchanged } from "typescript";
import { tsImportEqualsDeclaration } from "@babel/types";
import { OrderStatus } from "components/common";
import { putData } from "components/fetch";

export default class ReservationCreate extends React.Component {
    constructor(props) {
        super(props);
        let { id } = this.props.match.params;
        this.state = {
            orderId: id,
            voucherNo: '',
            customerName: '',
            customerNRC: '',
            customerPhone: '',
            customerEmail: '',
            allItems: [],
            allServices: [],
            allRooms: [],
            reservedRooms: [],
            roomItems: [],
            roomServices: [],
            itemOptions: [],
            itemCodeOptions: [],
            itemTotal: 0,
            addorder: 0,
            permitted: true,
            totalCharges: 0
        }

        this.addUnit = this.addUnit.bind(this);
        this.changeItem = this.changeItem.bind(this);
        this.calculateQtyInSmallestUnit = this.calculateQtyInSmallestUnit.bind(this);
        this.calculateAmount = this.calculateAmount.bind(this);
        this.calculateTotal = this.calculateTotal.bind(this);
        this.deleteItem = this.deleteItem.bind(this);
        this.changeService = this.changeService.bind(this);
        this.changeRoom = this.changeRoom.bind(this);

        Array.prototype.sum = function (prop) {
            var total = 0
            for (var i = 0, _len = this.length; i < _len; i++) {
                total += this[i][prop]
            }
            return total
        }
    }

    async componentDidMount() {
        var response = await getData('/api/roomtype/getItems');
        if (response.ok) {
            this.state.allItems = response.data;
            this.setState({ allItems: this.state.allItems });
            console.log(this.state.allItems);
            this.state.allItems.map((item, index) => {
                var option = { value: item.itemId, label: item.itemName };
                var codeOption = { value: item.itemId, label: item.itemCode };
                this.state.itemOptions.push(JSON.parse(JSON.stringify(option)));
                this.state.itemCodeOptions.push(JSON.parse(JSON.stringify(codeOption)));
                this.setState({ itemOptions: JSON.parse(JSON.stringify(this.state.itemOptions)) });
                this.setState({ itemCodeOptions: JSON.parse(JSON.stringify(this.state.itemCodeOptions)) });
            })
            console.log(this.state.itemOptions);
        } else {
            console.log(response.data);
        }

        var response = await getData('/api/roomtype/getServices');
        if (response.ok) {
            this.state.allServices = response.data;
            this.setState({ allServices: this.state.allServices });
            console.log(this.state.allServices);
        }

        var response = await getData('/api/room/getRoomsInfoForOrder');
        if (response.ok) {
            this.state.allRooms = response.data;
            this.setState({ allRooms: this.state.allRooms });
            console.log(this.state.allRooms);
        } else {
            console.log(response.data);
        }

        if (this.state.orderId != 0) {
            var response = await getData('/api/order/detail?id=' + this.state.orderId);
            if (response.ok) {
                var order = response.data;
                console.log(order)
                order.orderRooms.map((oroom, index) => {
                    var room = oroom.room;
                    var orderRoomItems = [];
                    oroom.orderRoomItems.map((item, index) => {
                        var roomItem = this.state.allItems.filter(x => x.itemId == item.itemId)[0];
                        roomItem.itemCode = { value: roomItem.itemId, label: roomItem.itemCode };
                        roomItem.itemName = { value: roomItem.itemId, label: roomItem.itemName };
                        roomItem.packingUnitID = item.unitID;
                        roomItem.unitPrice = item.unitPrice;
                        roomItem.quantity = item.quantity;
                        roomItem.amount = roomItem.unitPrice * roomItem.quantity;
                        roomItem.qtyInSmallestUnit = item.quantityInSmallestUnit;
                        orderRoomItems.push(roomItem);
                    }
                    )
                    var orderRoomServices = [];
                    oroom.orderRoomServices.map((service, index) => {
                        var roomService = this.state.allServices.filter(x => x.id == service.serviceId)[0];
                        orderRoomServices.push(roomService);
                    })
                    var startDate = oroom.startDate;
                    var endDate = oroom.endDate;
                    var amount = oroom.amount;
                    var status = oroom.status;

                    this.state.reservedRooms.push({ room, orderRoomItems, orderRoomServices, startDate, endDate, amount, status });
                    this.setState({ reservedRooms: this.state.reservedRooms });
                    this.calculateRoom(index);
                    this.calculateTotal(index);
                    this.calculateService(index);
                });

                console.log(this.state.reservedRooms);

                var voucherNo = order.voucherNo;
                var customerName = order.customerName;
                var customerEmail = order.customerEmail;
                var customerPhone = order.customerPhone;
                var customerNRC = order.customerNRC;

                this.setState({ voucherNo, customerName, customerName, customerPhone, customerNRC, customerEmail });
            }
        }
    }

    calculateTotalOrder() {
        var total = 0;
        this.state.reservedRooms.map((room, index) => {
            total += room.amount + (!isNaN(room.serviceTotal) && room.serviceTotal != null && room.serviceTotal != '' ? room.serviceTotal : 0) + (!isNaN(room.itemTotal) && room.itemTotal != null && room.itemTotal != '' ? room.itemTotal : 0);
        });

        this.setState({ totalCharges: total });
    }

    //room start
    addRoom() {
        if (this.state.permitted) {
            if (this.state.allRooms.length > 0) {
                this.state.reservedRooms.push(this.state.allRooms[0]);
                this.setState({ reservedRooms: this.state.reservedRooms });
                console.log(this.state.reservedRooms);
                var current = this.state.reservedRooms.length - 1;
                this.changeRoom(current, this.state.reservedRooms[current].room.id);
            }
        }
    }

    changeRoom(i, value) {
        this.state.reservedRooms[i] = this.state.allRooms.filter(x => x.room.id == value)[0];
        this.setState({ reservedRooms: this.state.reservedRooms });
        this.calculateRoom(i);
    }

    calculateRoom(i) {
        if (this.state.reservedRooms[i].startDate != "" && this.state.reservedRooms[i].startDate != null && this.state.reservedRooms[i].endDate != "" && this.state.reservedRooms[i].endDate != null) {
            var sDate = new Date(this.state.reservedRooms[i].startDate);
            var eDate = new Date(this.state.reservedRooms[i].endDate);
            var dDifference = Math.floor((eDate - sDate) / (1000 * 60 * 60 * 24));
            console.log(dDifference);
            this.state.reservedRooms[i].amount = this.state.reservedRooms[i].room.roomType.roomPrice * dDifference;

            this.setState({ reservedRooms: JSON.parse(JSON.stringify(this.state.reservedRooms)) });
            this.calculateTotalOrder();
        }
    }

    changeCheckinDate(i, e) {
        this.state.reservedRooms[i].startDate = e.target.value;
        this.setState({ reservedRooms: JSON.parse(JSON.stringify(this.state.reservedRooms)) });
        this.calculateRoom(i);
    }

    changeCheckoutDate(i, e) {
        this.state.reservedRooms[i].endDate = e.target.value;
        this.setState({ reservedRooms: JSON.parse(JSON.stringify(this.state.reservedRooms)) });
        this.calculateRoom(i);
    }

    changeStatus(ri, value) {
        this.state.reservedRooms[ri].status = value;
        this.setState({ reservedRooms: JSON.parse(JSON.stringify(this.state.reservedRooms)) });
    }

    deleteRoom(i) {
        this.state.reservedRooms.splice(i, 1);
        this.setState({ reservedRooms: JSON.parse(JSON.stringify(this.state.reservedRooms)) });
    }
    //room end

    changeItem(ri, i, value) {
        this.state.reservedRooms[ri].orderRoomItems[i] = this.state.allItems.filter(x => x.itemId == value)[0];
        this.state.reservedRooms[ri].orderRoomItems[i].packingUnitID = this.state.reservedRooms[ri].orderRoomItems[i].units[this.state.reservedRooms[ri].orderRoomItems[i].units.length - 1].id;
        this.state.reservedRooms[ri].orderRoomItems[i].itemCode = this.state.itemCodeOptions.filter(x => x.value == value)[0];
        this.state.reservedRooms[ri].orderRoomItems[i].itemName = this.state.itemOptions.filter(x => x.value == value)[0];
        this.setState({ reservedRooms: JSON.parse(JSON.stringify(this.state.reservedRooms)) });
        this.calculateAmount(ri, i);
        console.log(this.state.reservedRooms)
    }

    addUnit(ri) {
        if (this.state.permitted) {
            if (this.state.allItems.length > 0) {
                var sortorder = this.state.reservedRooms[ri].orderRoomItems.length + 1;
                this.state.reservedRooms[ri].orderRoomItems.push(JSON.parse(JSON.stringify(this.state.allItems[0])));
                // $('#unitid' + sortorder).val(this.state.roomItems[sortorder - 1].units[0].id);
                this.state.reservedRooms[ri].orderRoomItems[sortorder - 1].packingUnitID = this.state.reservedRooms[ri].orderRoomItems[sortorder - 1].units[this.state.reservedRooms[ri].orderRoomItems[sortorder - 1].units.length - 1].id;
                this.setState({ reservedRooms: JSON.parse(JSON.stringify(this.state.reservedRooms)) });
                // this.calculateAmount((sortorder - 1));
                this.setState({ addorder: this.state.addorder + 1 });
                //this.sortUnit();
                console.log(this.state.reservedRooms)
                this.changeItem(ri, sortorder - 1, this.state.reservedRooms[ri].orderRoomItems[sortorder - 1].itemId);
            }
        }
    }

    changeUnit(ri, i, value) {
        this.state.reservedRooms[ri].orderRoomItems[i].packingUnitID = value.value;
        this.setState({ reservedRooms: this.state.reservedRooms });
        this.calculateAmount(ri, i);
    }

    calculateAmount(ri, i) {
        this.state.reservedRooms[ri].orderRoomItems[i].qtyInSmallestUnit = this.calculateQtyInSmallestUnit(this.state.reservedRooms[ri].orderRoomItems[i]);
        this.state.reservedRooms[ri].orderRoomItems[i].unitPrice = parseInt(this.state.reservedRooms[ri].orderRoomItems[i].units.filter(m => m.id == this.state.reservedRooms[ri].orderRoomItems[i].packingUnitID)[0].saleamount);
        this.state.reservedRooms[ri].orderRoomItems[i].amount = this.state.reservedRooms[ri].orderRoomItems[i].unitPrice * parseInt(((!isNaN(this.state.reservedRooms[ri].orderRoomItems[i].quantity) && this.state.reservedRooms[ri].orderRoomItems[i].quantity != null && this.state.reservedRooms[ri].orderRoomItems[i].quantity != '') ? this.state.reservedRooms[ri].orderRoomItems[i].quantity : 0));
        this.setState({ reservedRooms: JSON.parse(JSON.stringify(this.state.reservedRooms)) });
        this.calculateTotal(ri);

    }

    calculateQtyInSmallestUnit(item) {
        var qty = 1;
        item.qtyInSmallestUnit = item.quantity;
        var currentUnit = item.units.find(function (element) {
            return element.id === item.packingUnitID;
        });
        for (var i = Math.max.apply(Math, item.units.map(function (o) { return o.unitOrder; })); i > currentUnit.unitOrder; i--) {
            qty *= item.units[i - 1].qtyInParent;
        }
        item.qtyInSmallestUnit *= qty;
        return item.qtyInSmallestUnit;
    }

    changeQty(ri, i, value) {
        this.state.reservedRooms[ri].orderRoomItems[i].quantity = value;
        this.calculateAmount(ri, i);
    }

    calculateTotal(ri) {
        this.state.reservedRooms[ri].itemTotal = this.state.reservedRooms[ri].orderRoomItems.sum("amount");
        this.setState({ reservedRooms: JSON.parse(JSON.stringify(this.state.reservedRooms)) });
        this.calculateTotalOrder();
    }

    deleteItem(ri, i) {
        this.state.reservedRooms[ri].orderRoomItems.splice(i, 1);
        this.setState({ reservedRooms: JSON.parse(JSON.stringify(this.state.reservedRooms)) });
        this.calculateTotal(ri);
    }

    addService(ri) {
        if (this.state.permitted) {
            if (this.state.allServices.length > 0) {
                var service = this.state.allServices[0];
                this.state.reservedRooms[ri].orderRoomServices.push(service);
                this.setState({ reservedRooms: JSON.parse(JSON.stringify(this.state.reservedRooms)) });
                this.calculateService(ri);
            }
        }
    }

    changeService(ri, i, value) {
        this.state.reservedRooms[ri].orderRoomServices[i] = this.state.allServices.filter(x => x.id == value)[0];
        this.setState({ reservedRooms: JSON.parse(JSON.stringify(this.state.reservedRooms)) });
        this.calculateService(ri)
    }

    deleteService(ri, i) {
        this.state.reservedRooms[ri].orderRoomServices.splice(i, 1);
        this.setState({ reservedRooms: JSON.parse(JSON.stringify(this.state.reservedRooms)) });
        this.calculateService(ri)
    }

    calculateService(ri) {
        this.state.reservedRooms[ri].serviceTotal = this.state.reservedRooms[ri].orderRoomServices.sum("charges");
        this.setState({ reservedRooms: JSON.parse(JSON.stringify(this.state.reservedRooms)) });
        this.calculateTotalOrder();
    }

    async save() {
        var rooms = [];
        this.state.reservedRooms.map((room, rindex) => {
            var orderRoomItems = [];
            room.orderRoomItems.map((item, index) => {
                var roomItem = ({ ItemId: item.itemId, UnitID: item.packingUnitID, UnitPrice: item.unitPrice, Quantity: item.quantity, QuantityInSmallestUnit: item.qtyInSmallestUnit, OrderRoomId: room.room.id });
                orderRoomItems.push(roomItem);
            })

            var orderRoomServices = [];
            room.orderRoomServices.map((service, index) => {
                var roomService = { ServiceId: service.id, OrderRoomId: room.room.id, Qty: 1, PricePerService: service.charges };
                orderRoomServices.push(roomService);
            })

            var room = { RoomId: room.room.id, RoomCharges: room.room.roomType.roomPrice, StartDate: room.startDate, EndDate: room.endDate, Status: room.status, ExtraBedQty: 0, OrderRoomItems: orderRoomItems, OrderRoomServices: orderRoomServices };
            rooms.push(room);
        })
        var model = {
            voucherNo: this.state.voucherNo,
            CustomerName: this.state.customerName,
            CustomerNRC: this.state.customerNRC,
            CustomerPhone: this.state.customerPhone,
            CustomerEmail: this.state.customerEmail,
            OrderRooms: rooms,
            TotalCharges: this.state.totalCharges
        };

        if (this.state.orderId == 0) {
            var response = await postData('/api/order/create', model);
            if (response.ok) {
                window.location.href = "/admin/reservation/index";
            } else {
                console.log(response.data);
            }
        }else{
            var response = await putData('/api/order/edit?id='+this.state.orderId, model);
            if(response.ok){
                window.location.href = "/admin/reservation/index";
            }else{
                console.log(response.data);
            }
        }


    }

    render() {
        return (
            <>
                <GridContainer style={{ width: 1000 }}>
                    <GridItem md={6}>
                        <GridContainer>
                            <GridItem md={4}>
                                <label>Voucher No</label>
                            </GridItem>
                            <GridItem md={8}>
                                <Input label="Name" type="text" style={{ width: '100%' }} value={this.state.voucherNo} onChange={(event) => this.setState({ voucherNo: event.target.value })}></Input>
                            </GridItem>
                        </GridContainer>

                    </GridItem>
                    <GridItem md={6}>
                        <GridContainer>
                            <GridItem md={4}>
                                <label>Customer Name</label>
                            </GridItem>
                            <GridItem md={8}>
                                <Input label="AvailableBedQty" type="text" style={{ width: '100%' }} value={this.state.customerName} onChange={(event) => this.setState({ customerName: event.target.value })}></Input>
                            </GridItem>
                        </GridContainer>
                    </GridItem>
                </GridContainer>
                <br></br>
                <GridContainer style={{ width: 1000 }}>
                    <GridItem md={6}>
                        <GridContainer>
                            <GridItem md={4}>
                                <label>Customer Phone</label>
                            </GridItem>
                            <GridItem md={8}>
                                <Input label="Name" type="tel" style={{ width: '100%' }} value={this.state.customerPhone} onChange={(event) => this.setState({ customerPhone: event.target.value })}></Input>
                            </GridItem>
                        </GridContainer>

                    </GridItem>
                    <GridItem md={6}>
                        <GridContainer>
                            <GridItem md={4}>
                                <label>Customer Email</label>
                            </GridItem>
                            <GridItem md={8}>
                                <Input label="Charges" type="email" style={{ width: '100%' }} value={this.state.customerEmail} onChange={(event) => this.setState({ customerEmail: event.target.value })}></Input>
                            </GridItem>
                        </GridContainer>
                    </GridItem>
                </GridContainer>
                <br></br>
                <GridContainer style={{ width: 1000 }}>
                    <GridItem md={6}>
                        <GridContainer>
                            <GridItem md={4}>
                                <label>Customer NRC</label>
                            </GridItem>
                            <GridItem md={8}>
                                <Input label="Name" type="text" style={{ width: '100%' }} value={this.state.customerNRC} onChange={(event) => this.setState({ customerNRC: event.target.value })}></Input>
                            </GridItem>
                        </GridContainer>

                    </GridItem>
                    <GridItem md={6}>
                        <GridContainer>
                            <GridItem md={4}>
                                <label>Total Charges</label>
                            </GridItem>
                            <GridItem md={8}>
                                <Input value={this.state.totalCharges + " ks"} type='text' readOnly></Input>
                            </GridItem>
                        </GridContainer>

                    </GridItem>
                </GridContainer>
                <br></br>
                <h4>Rooms</h4>
                <Table>
                    <TableHead>
                        <TableRow style={{ backgroundColor: 'black', color: 'white' }}>
                            <TableCell style={{ color: 'white' }}>No</TableCell>
                            <TableCell style={{ color: 'white' }}>RoomNo</TableCell>
                            <TableCell style={{ color: 'white' }}>Checkin Date</TableCell>
                            <TableCell style={{ color: 'white' }}>Early Checkin</TableCell>
                            <TableCell style={{ color: 'white' }}>Checkout Date</TableCell>
                            <TableCell style={{ color: 'white' }}>PricePerDay</TableCell>
                            <TableCell style={{ color: 'white' }}>Amount</TableCell>
                            <TableCell style={{ color: 'white' }}>
                                <ButtonBase color="white" onClick={() => this.addRoom()}>
                                    <i class="material-icons md-48" color="white">add_box</i>
                                </ButtonBase>
                            </TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody style={{ backgroundColor: 'white' }}>
                        {
                            this.state.reservedRooms.map((room, rindex) =>
                                <>
                                    <TableRow>
                                        <TableCell>{parseInt(rindex + 1)}.</TableCell>
                                        <TableCell>
                                            <Select onChange={(value) => this.changeRoom(rindex, value.value)} value={{ value: room.room.id, label: this.state.allRooms.filter(x => x.room.id == room.room.id)[0] != null ? this.state.allRooms.filter(x => x.room.id == room.room.id)[0].room.roomNo : '' }} options={this.state.allRooms.map((r, ri) => { return { value: r.room.id, label: r.room.roomNo } })}></Select>
                                        </TableCell>
                                        <TableCell><TextField style={{ width: 200 }} onChange={(event) => this.changeCheckinDate(rindex, event)} type='datetime-local' value={room.startDate}></TextField> </TableCell>
                                        <TableCell><Checkbox color='default'></Checkbox></TableCell>
                                        <TableCell><TextField style={{ width: 200 }} onChange={(event) => this.changeCheckoutDate(rindex, event)} type='datetime-local' value={room.endDate}></TextField> </TableCell>
                                        <TableCell>{room.room.roomType.roomPrice}</TableCell>
                                        <TableCell>{room.amount}</TableCell>
                                        <TableCell>
                                            <ButtonBase color="white" onClick={() => this.deleteRoom(rindex)}>
                                                <i class="material-icons md-48">highlight_off</i>
                                            </ButtonBase>
                                        </TableCell>
                                    </TableRow>
                                    <TableRow>
                                        <TableCell colSpan={2}></TableCell>
                                        <TableCell><Button color='info' onClick={() => this.addUnit(rindex)}>Add Items</Button></TableCell>
                                        <TableCell><Button color='info' onClick={() => this.addService(rindex)}>Add Services</Button></TableCell>
                                        <TableCell colSpan={4}></TableCell>
                                    </TableRow>
                                    {
                                        room.orderRoomItems.length > 0 ?
                                            <TableRow>
                                                <TableCell colSpan={1}></TableCell>
                                                <TableCell colSpan={7}>
                                                    {/* <p>Items</p> */}
                                                    <Table>
                                                        <TableHead>
                                                            <TableRow style={{ backgroundColor: 'black', color: 'white' }}>
                                                                <TableCell style={{ color: 'white' }}>No</TableCell>
                                                                <TableCell style={{ color: 'white' }}>Code</TableCell>
                                                                <TableCell style={{ color: 'white' }}>Items</TableCell>
                                                                <TableCell style={{ color: 'white' }}>Unit</TableCell>
                                                                <TableCell style={{ color: 'white' }}>UnitPrice</TableCell>
                                                                <TableCell style={{ color: 'white' }}>Qty</TableCell>
                                                                <TableCell style={{ color: 'white' }}>Amount</TableCell>
                                                                <TableCell style={{ color: 'white' }}>
                                                                    <ButtonBase color="white" onClick={() => this.addUnit(rindex)}>
                                                                        <i class="material-icons md-48" color="white">add_box</i>
                                                                    </ButtonBase>
                                                                </TableCell>
                                                            </TableRow>
                                                        </TableHead>
                                                        <TableBody>
                                                            {
                                                                room.orderRoomItems.map((item, index) =>
                                                                    <TableRow>
                                                                        <TableCell>{parseInt(index + 1)}.</TableCell>
                                                                        <TableCell><Select options={this.state.itemCodeOptions} value={item.itemCode} onChange={(value) => this.changeItem(rindex, index, value.value)}></Select></TableCell>
                                                                        <TableCell><Select options={this.state.itemOptions} value={item.itemName} onChange={(value) => this.changeItem(rindex, index, value.value)}></Select></TableCell>
                                                                        <TableCell><Select options={item.units.map((unit, index) => { return { value: unit.id, label: unit.name } })} value={{ value: item.units.filter(x => x.id == item.packingUnitID)[0].id, label: item.units.filter(x => x.id == item.packingUnitID)[0].name }} onChange={(value) => this.changeUnit(rindex, index, value)}></Select></TableCell>
                                                                        <TableCell>{item.unitPrice}</TableCell>
                                                                        <TableCell><Input style={{ width: 100 }} type="number" value={item.quantity} onChange={(event) => this.changeQty(rindex, index, event.target.value)}></Input></TableCell>
                                                                        <TableCell>{item.amount}</TableCell>
                                                                        <TableCell>
                                                                            <ButtonBase color="white" onClick={() => this.deleteItem(rindex, index)}>
                                                                                <i class="material-icons md-48">highlight_off</i>
                                                                            </ButtonBase>
                                                                        </TableCell>
                                                                    </TableRow>)
                                                            }
                                                            <TableRow>
                                                                <TableCell colSpan={5}></TableCell>
                                                                <TableCell>Total</TableCell>
                                                                <TableCell>{room.itemTotal}</TableCell>
                                                            </TableRow>
                                                        </TableBody>
                                                    </Table>
                                                </TableCell>
                                            </TableRow> : <></>
                                    }
                                    {
                                        room.orderRoomServices.length > 0 ?
                                            <TableRow>
                                                <TableCell colSpan={3}></TableCell>
                                                <TableCell colSpan={5}>
                                                    {/* <p>Services</p> */}
                                                    <Table>
                                                        <TableHead>
                                                            <TableRow style={{ backgroundColor: 'black', color: 'white' }}>
                                                                <TableCell style={{ color: 'white' }}>No</TableCell>
                                                                <TableCell style={{ color: 'white' }}>Services</TableCell>
                                                                <TableCell style={{ color: 'white' }}>Charges</TableCell>
                                                                <TableCell style={{ color: 'white' }}>
                                                                    <ButtonBase color="white" onClick={() => this.addService(rindex)}>
                                                                        <i class="material-icons md-48" color="white">add_box</i>
                                                                    </ButtonBase>
                                                                </TableCell>
                                                            </TableRow>
                                                        </TableHead>
                                                        <TableBody>
                                                            {
                                                                room.orderRoomServices.map((service, index) =>
                                                                    <TableRow>
                                                                        <TableCell>{parseInt(index + 1)}</TableCell>
                                                                        <TableCell>
                                                                            <Select options={this.state.allServices.map((s, i) => { return { value: s.id, label: s.name } })} value={{ value: service.id, label: service.name }} onChange={(value) => this.changeService(rindex, index, value.value)}></Select>
                                                                        </TableCell>
                                                                        <TableCell>{service.charges}</TableCell>
                                                                        <TableCell>
                                                                            <ButtonBase color="white" onClick={() => this.deleteService(rindex, index)}>
                                                                                <i class="material-icons md-48">highlight_off</i>
                                                                            </ButtonBase>
                                                                        </TableCell>
                                                                    </TableRow>)
                                                            }
                                                            <TableRow>
                                                                <TableCell colSpan={2}></TableCell>
                                                                <TableCell>Total</TableCell>
                                                                <TableCell>{room.serviceTotal}</TableCell>
                                                            </TableRow>
                                                        </TableBody>
                                                    </Table>
                                                </TableCell>
                                            </TableRow> : <></>
                                    }

                                    <TableRow>
                                        <TableCell colSpan={4}></TableCell>
                                        <TableCell><Select onChange={(value) => this.changeStatus(rindex, value.value)} value={{ value: room.status, label: OrderStatus[room.status - 1] }} options={OrderStatus.map((status, index) => { return { value: parseInt(index + 1), label: status } })}></Select></TableCell>
                                        <TableCell></TableCell>
                                        <TableCell><h4>{room.amount + (!isNaN(room.serviceTotal) && room.serviceTotal != null && room.serviceTotal != '' ? room.serviceTotal : 0) + (!isNaN(room.itemTotal) && room.itemTotal != null && room.itemTotal != '' ? room.itemTotal : 0)}</h4></TableCell>
                                        <TableCell></TableCell>
                                    </TableRow>
                                </>
                            )
                        }
                    </TableBody>
                </Table>

                <br></br>

                <br></br>
                <br></br>
                <Button color="info" onClick={() => this.save()}>Create</Button>
            </>
        )
    }
}