import React from "react";
import PropTypes from "prop-types";
// @material-ui/core components
import Table from "components/Table/Table.js";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import Button from "components/CustomButtons/Button.js";
import { NavLink } from "react-router-dom";
import { Switch, Route, Redirect } from "react-router-dom";
import Create from '../Item/create.js';
import { getData } from "components/fetch.js";
import { OrderStatus } from "components/common.js";

export default class ReservationIndex extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            orders: []
        }
    }

    async componentDidMount() {
        var response = await getData('/api/order/index');
        if (response.ok) {
            this.state.orders = response.data;
            this.setState({ orders: this.state.orders });
            console.log(this.state.orders);
        } else {
            console.log(response.data);
        }
    }

    render() {
        return (
            <>
                <NavLink to='/admin/reservation/create/0'>
                    <Button color="info">Create New</Button>
                </NavLink>
                <Table
                    tableHeaderColor="info"
                    tableHead={["VoucherNo", "Name", "Phone", "Email", "TotalRooms", "TotalCharges", ""]}>
                    {
                        this.state.orders.map((order, index) =>
                            <TableRow>
                                <TableCell>{order.voucherNo}</TableCell>
                                <TableCell>{order.customerName}</TableCell>
                                <TableCell>{order.customerPhone}</TableCell>
                                <TableCell>{order.customerEmail}</TableCell>
                                <TableCell>{order.orderRooms.length}</TableCell>
                                <TableCell>{order.totalCharges}</TableCell>
                                <TableCell>
                                    <NavLink to={'/admin/reservation/create/'+order.id}>
                                        <Button color="info">Edit</Button>
                                    </NavLink>
                                </TableCell>
                            </TableRow>
                        )
                    }

                </Table>
            </>
        )
    }
}