import React from "react";
import PropTypes from "prop-types";
// @material-ui/core components
import Table from "components/Table/Table.js";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import Button from "components/CustomButtons/Button.js";
import { NavLink } from "react-router-dom";
import { Switch, Route, Redirect } from "react-router-dom";
import Create from '../Item/create.js';
import { getData } from "components/fetch.js";

export default class RoomTypeIndex extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            roomTypes : []
        }
    }

    async componentDidMount(){
        var response = await getData('/api/roomtype/index');
        if(response.ok){
            this.state.roomTypes = response.data;
            this.setState({roomTypes:this.state.roomTypes});
            console.log(this.state.roomTypes);
        }else{
            console.log(response.data);
        }
    }

    render() {
        return (
            <>
                <NavLink to='/admin/roomtype/create'>
                    <Button color="info">Create New</Button>
                </NavLink>
                <Table
                    tableHeaderColor="info"
                    tableHead={["Name", "Price", "AvailableBed", "AvailableExtBed", "PricePerBed", "PricePerExtBed", ""]}>
                    {
                        this.state.roomTypes.map((item, index) =>
                            <TableRow>
                                <TableCell>{item.name}</TableCell>
                                <TableCell>{item.roomPrice}</TableCell>
                                <TableCell>{item.availableBedQty}</TableCell>
                                <TableCell>{item.availableExtraBedQty}</TableCell>
                                <TableCell>{item.pricePerBed}</TableCell>
                                <TableCell>{item.pricePerExtraBed}</TableCell>
                                <TableCell>
                                    <Button color="info">View</Button>
                                </TableCell>
                            </TableRow>
                        )
                    }

                </Table>
            </>
        )
    }
}