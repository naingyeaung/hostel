import React from "react";
import { MenuItem, FormControl, Grid, Input, Table, TableHead, TableBody, ButtonBase } from "@material-ui/core";
import Select from 'react-select'
import Button from "components/CustomButtons/Button.js";
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import { getData } from "components/fetch";
import { postData } from "components/fetch";
import Checkbox from '@material-ui/core/Checkbox';
import { textChangeRangeIsUnchanged } from "typescript";
import { tsImportEqualsDeclaration } from "@babel/types";

export default class RoomTypeCreate extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            avaBedQty: '',
            avaExtBedQty: '',
            roomPrice: '',
            pricePerBed: '',
            pricePerExtraBed: '',
            isSharable: false,
            allItems: [],
            allServices: [],
            roomItems: [],
            roomServices: [],
            itemOptions: [],
            itemCodeOptions: [],
            itemTotal: 0,
            addorder: 0,
            permitted: true
        }

        this.addUnit = this.addUnit.bind(this);
        this.changeItem = this.changeItem.bind(this);
        this.calculateQtyInSmallestUnit = this.calculateQtyInSmallestUnit.bind(this);
        this.calculateAmount = this.calculateAmount.bind(this);
        this.calculateTotal = this.calculateTotal.bind(this);
        this.deleteItem = this.deleteItem.bind(this);
        this.changeService = this.changeService.bind(this);

        Array.prototype.sum = function (prop) {
            var total = 0
            for (var i = 0, _len = this.length; i < _len; i++) {
                total += this[i][prop]
            }
            return total
        }
    }

    async componentDidMount() {
        var response = await getData('/api/roomtype/getItems');
        if (response.ok) {
            this.state.allItems = response.data;
            this.setState({ allItems: this.state.allItems });
            console.log(this.state.allItems);
            this.state.allItems.map((item, index) => {
                var option = { value: item.itemId, label: item.itemName };
                var codeOption = { value: item.itemId, label: item.itemCode };
                this.state.itemOptions.push(JSON.parse(JSON.stringify(option)));
                this.state.itemCodeOptions.push(JSON.parse(JSON.stringify(codeOption)));
                this.setState({ itemOptions: JSON.parse(JSON.stringify(this.state.itemOptions)) });
                this.setState({ itemCodeOptions: JSON.parse(JSON.stringify(this.state.itemCodeOptions)) });
            })
            console.log(this.state.itemOptions);
        } else {
            console.log(response.data);
        }

        var response = await getData('/api/roomtype/getServices');
        if (response.ok) {
            this.state.allServices = response.data;
            this.setState({ allServices: this.state.allServices });
            console.log(this.state.allServices);
        }
    }

    changeItem(i, value) {
        this.state.roomItems[i] = this.state.allItems.filter(x => x.itemId == value)[0];
        this.state.roomItems[i].packingUnitID = this.state.roomItems[i].units[this.state.roomItems[i].units.length - 1].id;
        this.state.roomItems[i].itemCode = this.state.itemCodeOptions.filter(x => x.value == value)[0];
        this.state.roomItems[i].itemName = this.state.itemOptions.filter(x => x.value == value)[0];
        this.setState({ roomItems: JSON.parse(JSON.stringify(this.state.roomItems)) });
        this.calculateAmount(i);
        console.log(this.state.roomItems)
    }

    addUnit() {
        if (this.state.permitted) {
            if (this.state.allItems.length > 0) {
                var sortorder = this.state.roomItems.length + 1;
                this.state.roomItems.push(JSON.parse(JSON.stringify(this.state.allItems[0])));
                // $('#unitid' + sortorder).val(this.state.roomItems[sortorder - 1].units[0].id);
                this.state.roomItems[sortorder - 1].packingUnitID = this.state.roomItems[sortorder - 1].units[this.state.roomItems[sortorder - 1].units.length - 1].id;
                this.setState({ roomItems: JSON.parse(JSON.stringify(this.state.roomItems)) });
                // this.calculateAmount((sortorder - 1));
                this.setState({ addorder: this.state.addorder + 1 });
                //this.sortUnit();
                console.log(this.state.roomItems)
                this.changeItem(sortorder - 1, this.state.roomItems[sortorder - 1].itemId);
            }
        }
    }

    changeUnit(i, value) {
        this.state.roomItems[i].packingUnitID = value.value;
        this.setState({ roomItems: this.state.roomItems });
        this.calculateAmount(i);
    }

    calculateAmount(i) {
        this.state.roomItems[i].qtyInSmallestUnit = this.calculateQtyInSmallestUnit(this.state.roomItems[i]);
        this.state.roomItems[i].unitPrice = parseInt(this.state.roomItems[i].units.filter(m => m.id == this.state.roomItems[i].packingUnitID)[0].saleamount);
        this.state.roomItems[i].amount = this.state.roomItems[i].unitPrice * parseInt(((!isNaN(this.state.roomItems[i].qty) && this.state.roomItems[i].qty != null && this.state.roomItems[i].qty != '') ? this.state.roomItems[i].qty : 0));
        this.setState({ roomItems: this.state.roomItems });
        this.calculateTotal();
    }

    calculateQtyInSmallestUnit(item) {
        var qty = 1;
        item.qtyInSmallestUnit = item.qty;
        var currentUnit = item.units.find(function (element) {
            return element.id === item.packingUnitID;
        });
        for (var i = Math.max.apply(Math, item.units.map(function (o) { return o.unitOrder; })); i > currentUnit.unitOrder; i--) {
            qty *= item.units[i - 1].qtyInParent;
        }
        item.qtyInSmallestUnit *= qty;
        return item.qtyInSmallestUnit;
    }

    changeQty(i, value) {
        this.state.roomItems[i].qty = value;
        this.calculateAmount(i);
    }

    calculateTotal() {
        this.state.itemTotal = this.state.roomItems.sum("amount");
        this.setState({ itemTotal: this.state.itemTotal });
    }

    deleteItem(i) {
        this.state.roomItems.splice(i, 1);
        this.setState({ roomItems: this.state.roomItems });
        this.calculateTotal();
    }

    addService() {
        if (this.state.permitted) {
            if (this.state.allServices.length > 0) {
                var service = this.state.allServices[0];
                this.state.roomServices.push(service);
                this.setState({ allServices: this.state.allServices });
            }
        }
    }

    changeService(i, value) {
        this.state.roomServices[i] = this.state.allServices.filter(x => x.id == value)[0];
        this.setState({ roomServices: this.state.roomServices });
    }

    deleteService(i) {
        this.state.roomServices.splice(i, 1);
        this.setState({ roomServices: this.state.roomServices });
    }

    async save() {
        var roomItems = [];
        this.state.roomItems.map((item, index) => {
            var roomItem = ({ ItemId: item.itemId, UnitID: item.packingUnitID, UnitPrice: item.unitPrice, Quantity: item.qty, QuantityInSmallestUnit: item.qtyInSmallestUnit });
            roomItems.push(roomItem);
        })

        var roomServices = [];
        this.state.roomServices.map((service, index) => {
            var roomService = { ServiceId: service.id };
            roomServices.push(roomService);
        })

        var model = {
            Name: this.state.name,
            AvailableBedQty: this.state.avaBedQty,
            AvailableExtraBedQty: this.state.avaExtBedQty,
            RoomPrice: this.state.roomPrice,
            PricePerBed: this.state.pricePerBed,
            PricePerExtraBed: this.state.pricePerExtraBed,
            RoomTypeItems: roomItems,
            RoomTypeServices: roomServices,
            IsSharable: this.state.isSharable
        }

        console.log(model);
        var response = await postData('/api/roomtype/create', model);
        if (response.ok) {
            window.location.href = "/admin/roomtype/index";
        } else {
            console.log(response.data);
        }
    }

    render() {
        return (
            <>
                <GridContainer style={{ width: 1000 }}>
                    <GridItem md={6}>
                        <GridContainer>
                            <GridItem md={4}>
                                <label>Name</label>
                            </GridItem>
                            <GridItem md={8}>
                                <Input label="Name" type="text" style={{ width: '100%' }} value={this.state.name} onChange={(event) => this.setState({ name: event.target.value })}></Input>
                            </GridItem>
                        </GridContainer>

                    </GridItem>
                    <GridItem md={6}>
                        <GridContainer>
                            <GridItem md={4}>
                                <label>Available Bed Qty</label>
                            </GridItem>
                            <GridItem md={8}>
                                <Input label="AvailableBedQty" type="number" style={{ width: '100%' }} value={this.state.avaBedQty} onChange={(event) => this.setState({ avaBedQty: event.target.value })}></Input>
                            </GridItem>
                        </GridContainer>
                    </GridItem>
                </GridContainer>
                <br></br>
                <GridContainer style={{ width: 1000 }}>
                    <GridItem md={6}>
                        <GridContainer>
                            <GridItem md={4}>
                                <label>Available Extra Bed Qty</label>
                            </GridItem>
                            <GridItem md={8}>
                                <Input label="Name" type="number" style={{ width: '100%' }} value={this.state.avaExtBedQty} onChange={(event) => this.setState({ avaExtBedQty: event.target.value })}></Input>
                            </GridItem>
                        </GridContainer>

                    </GridItem>
                    <GridItem md={6}>
                        <GridContainer>
                            <GridItem md={4}>
                                <label>Price Per Bed</label>
                            </GridItem>
                            <GridItem md={8}>
                                <Input label="Charges" type="number" style={{ width: '100%' }} value={this.state.pricePerBed} onChange={(event) => this.setState({ pricePerBed: event.target.value })}></Input>
                            </GridItem>
                        </GridContainer>
                    </GridItem>
                </GridContainer>
                <br></br>
                <GridContainer style={{ width: 1000 }}>
                    <GridItem md={6}>
                        <GridContainer>
                            <GridItem md={4}>
                                <label>Price Per Extra Bed</label>
                            </GridItem>
                            <GridItem md={8}>
                                <Input label="Name" type="number" style={{ width: '100%' }} value={this.state.pricePerExtraBed} onChange={(event) => this.setState({ pricePerExtraBed: event.target.value })}></Input>
                            </GridItem>
                        </GridContainer>

                    </GridItem>
                    <GridItem md={6}>
                        <GridContainer>
                            <GridItem md={4}>
                                <label>Is Sharable</label>
                            </GridItem>
                            <GridItem md={8}>
                                <Checkbox value={this.state.isSharable} color="default" onChange={(value) => this.setState({ isSharable: value })}></Checkbox>
                            </GridItem>
                        </GridContainer>
                    </GridItem>
                </GridContainer>
                <br></br>
                <GridContainer style={{ width: 1000 }}>
                    <GridItem md={6}>
                        <GridContainer>
                            <GridItem md={4}>
                                <label>Room Price</label>
                            </GridItem>
                            <GridItem md={8}>
                                <Input label="Name" type="number" style={{ width: '100%' }} value={this.state.roomPrice} onChange={(event) => this.setState({ roomPrice: event.target.value })}></Input>
                            </GridItem>
                        </GridContainer>
                    </GridItem>
                </GridContainer>
                <br></br>
                <h4>Items</h4>
                <Table>
                    <TableHead>
                        <TableRow style={{ backgroundColor: 'black', color: 'white' }}>
                            <TableCell style={{ color: 'white' }}>No</TableCell>
                            <TableCell style={{ color: 'white' }}>Code</TableCell>
                            <TableCell style={{ color: 'white' }}>Items</TableCell>
                            <TableCell style={{ color: 'white' }}>Unit</TableCell>
                            <TableCell style={{ color: 'white' }}>UnitPrice</TableCell>
                            <TableCell style={{ color: 'white' }}>Qty</TableCell>
                            <TableCell style={{ color: 'white' }}>Amount</TableCell>
                            <TableCell style={{ color: 'white' }}>
                                <ButtonBase color="white" onClick={() => this.addUnit()}>
                                    <i class="material-icons md-48" color="white">add_box</i>
                                </ButtonBase>
                            </TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {
                            this.state.roomItems.map((item, index) =>
                                <TableRow>
                                    <TableCell>{parseInt(index + 1)}.</TableCell>
                                    <TableCell><Select options={this.state.itemCodeOptions} value={item.itemCode} onChange={(value) => this.changeItem(index, value.value)}></Select></TableCell>
                                    <TableCell><Select options={this.state.itemOptions} value={item.itemName} onChange={(value) => this.changeItem(index, value.value)}></Select></TableCell>
                                    <TableCell><Select options={item.units.map((unit, index) => { return { value: unit.id, label: unit.name } })} value={{ value: item.units.filter(x => x.id == item.packingUnitID)[0].id, label: item.units.filter(x => x.id == item.packingUnitID)[0].name }} onChange={(value) => this.changeUnit(index, value)}></Select></TableCell>
                                    <TableCell>{item.unitPrice}</TableCell>
                                    <TableCell><Input style={{ width: 100 }} type="number" value={item.qty} onChange={(event) => this.changeQty(index, event.target.value)}></Input></TableCell>
                                    <TableCell>{item.amount}</TableCell>
                                    <TableCell>
                                        <ButtonBase color="white" onClick={() => this.deleteItem(index)}>
                                            <i class="material-icons md-48">highlight_off</i>
                                        </ButtonBase>
                                    </TableCell>
                                </TableRow>)
                        }
                        <TableRow>
                            <TableCell colSpan={5}></TableCell>
                            <TableCell>Total</TableCell>
                            <TableCell>{this.state.itemTotal}</TableCell>
                        </TableRow>
                    </TableBody>
                </Table>
                <br></br>
                <h4>Services</h4>
                <Table style={{ width: 1000 }}>
                    <TableHead>
                        <TableRow style={{ backgroundColor: 'black', color: 'white' }}>
                            <TableCell style={{ color: 'white' }}>No</TableCell>
                            <TableCell style={{ color: 'white' }}>Services</TableCell>
                            <TableCell style={{ color: 'white' }}>Charges</TableCell>
                            <TableCell style={{ color: 'white' }}>
                                <ButtonBase color="white" onClick={() => this.addService()}>
                                    <i class="material-icons md-48" color="white">add_box</i>
                                </ButtonBase>
                            </TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {
                            this.state.roomServices.map((service, index) =>
                                <TableRow>
                                    <TableCell>{parseInt(index + 1)}</TableCell>
                                    <TableCell>
                                        <Select options={this.state.allServices.map((s, i) => { return { value: s.id, label: s.name } })} value={{ value: service.id, label: service.name }} onChange={(value) => this.changeService(index, value.value)}></Select>
                                    </TableCell>
                                    <TableCell>{service.charges}</TableCell>
                                    <TableCell>
                                        <ButtonBase color="white" onClick={() => this.deleteService(index)}>
                                            <i class="material-icons md-48">highlight_off</i>
                                        </ButtonBase>
                                    </TableCell>
                                </TableRow>)
                        }
                        <TableRow>
                            <TableCell colSpan={2}></TableCell>
                            <TableCell>Total</TableCell>
                        </TableRow>
                    </TableBody>
                </Table>
                <br></br>
                <br></br>
                <Button color="info" onClick={() => this.save()}>Create</Button>
            </>
        )
    }
}