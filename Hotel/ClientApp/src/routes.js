/*!

=========================================================
* Material Dashboard React - v1.8.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/material-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
// @material-ui/icons
import Dashboard from "@material-ui/icons/Dashboard";
import Person from "@material-ui/icons/Person";
import LibraryBooks from "@material-ui/icons/LibraryBooks";
import BubbleChart from "@material-ui/icons/BubbleChart";
import LocationOn from "@material-ui/icons/LocationOn";
import Notifications from "@material-ui/icons/Notifications";
import Unarchive from "@material-ui/icons/Unarchive";
import Language from "@material-ui/icons/Language";
// core components/views for Admin layout
import DashboardPage from "views/Dashboard/Dashboard.js";
import UserProfile from "views/UserProfile/UserProfile.js";
import TableList from "views/TableList/TableList.js";
import Typography from "views/Typography/Typography.js";
import Icons from "views/Icons/Icons.js";
import Maps from "views/Maps/Maps.js";
import NotificationsPage from "views/Notifications/Notifications.js";
import UpgradeToPro from "views/UpgradeToPro/UpgradeToPro.js";
import ItemIndex from "views/Item/index.js"
import ItemCreate from "views/Item/create.js"
import ServiceIndex from "views/Service/index.js"
import ServiceCreate from "views/Service/create.js"
// core components/views for RTL layout
import RTLPage from "views/RTLPage/RTLPage.js";
import RoomTypeIndex from "views/RoomType";
import RoomTypeCreate from "views/RoomType/create";
import RoomIndex from "views/Room";
import RoomCreate from "views/Room/create";
import ReservationCreate from "views/Reservations/create";
import ReservationIndex from "views/Reservations";

const dashboardRoutes = [
  // {
  //   path: "/dashboard",
  //   name: "Dashboard",
  //   rtlName: "لوحة القيادة",
  //   icon: Dashboard,
  //   component: DashboardPage,
  //   layout: "/admin"
  // },
  // {
  //   path: "/user",
  //   name: "User Profile",
  //   rtlName: "ملف تعريفي للمستخدم",
  //   icon: Person,
  //   component: UserProfile,
  //   layout: "/admin"
  // },
  {
    path: "/item/index",
    name: "Item",
    rtlName: "قائمة الجدول",
    icon: "content_paste",
    component: ItemIndex,
    layout: "/admin"
  },
  {
    path: "/item/create/:id",
    name: "ItemCreate",
    rtlName: "قائمة الجدول",
    icon: "content_paste",
    component: ItemCreate,
    layout: "/admin"
  },
  {
    path: "/service/index",
    name: "Service",
    rtlName: "قائمة الجدول",
    icon: "rowing",
    component: ServiceIndex,
    layout: "/admin"
  },
  {
    path: "/service/create",
    name: "ServiceCreate",
    rtlName: "قائمة الجدول",
    icon: "content_paste",
    component: ServiceCreate,
    layout: "/admin"
  },
  {
    path: "/roomtype/index",
    name: "RoomType",
    rtlName: "قائمة الجدول",
    icon: "settings_applications",
    component: RoomTypeIndex,
    layout: "/admin"
  },
  {
    path: "/roomtype/create",
    name: "RoomTypeCreate",
    rtlName: "قائمة الجدول",
    icon: "content_paste",
    component: RoomTypeCreate,
    layout: "/admin"
  },
  {
    path: "/room/index",
    name: "Room",
    rtlName: "قائمة الجدول",
    icon: "view_carousel",
    component: RoomIndex,
    layout: "/admin"
  },
  {
    path: "/room/create",
    name: "RoomCreate",
    rtlName: "قائمة الجدول",
    icon: "content_paste",
    component: RoomCreate,
    layout: "/admin"
  },
  {
    path: "/reservation/index",
    name: "Reservation",
    rtlName: "قائمة الجدول",
    icon: "calendar_today",
    component: ReservationIndex,
    layout: "/admin"
  },
  {
    path: "/reservation/create/:id",
    name: "ReservationCreate",
    rtlName: "قائمة الجدول",
    icon: "content_paste",
    component: ReservationCreate,
    layout: "/admin"
  },
  // {
  //   path: "/typography",
  //   name: "Typography",
  //   rtlName: "طباعة",
  //   icon: LibraryBooks,
  //   component: Typography,
  //   layout: "/admin"
  // },
  {
    path: "/icons",
    name: "Icons",
    rtlName: "الرموز",
    icon: BubbleChart,
    component: Icons,
    layout: "/admin"
  },
  // {
  //   path: "/maps",
  //   name: "Maps",
  //   rtlName: "خرائط",
  //   icon: LocationOn,
  //   component: Maps,
  //   layout: "/admin"
  // },
  // {
  //   path: "/notifications",
  //   name: "Notifications",
  //   rtlName: "إخطارات",
  //   icon: Notifications,
  //   component: NotificationsPage,
  //   layout: "/admin"
  // }
];

export default dashboardRoutes;
