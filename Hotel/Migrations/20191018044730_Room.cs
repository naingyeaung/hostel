﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Hotel.Migrations
{
    public partial class Room : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MainStoreItem_Items_ItemId",
                table: "MainStoreItem");

            migrationBuilder.DropForeignKey(
                name: "FK_MainStoreItem_MainStore_MainStoreId",
                table: "MainStoreItem");

            migrationBuilder.DropForeignKey(
                name: "FK_PackingUnit_Items_ItemId",
                table: "PackingUnit");

            migrationBuilder.DropForeignKey(
                name: "FK_PackingUnit_Unit_UnitID",
                table: "PackingUnit");

            migrationBuilder.DropForeignKey(
                name: "FK_TransferItem_Items_ItemId",
                table: "TransferItem");

            migrationBuilder.DropForeignKey(
                name: "FK_TransferItem_TransferRecord_TransferRecordId",
                table: "TransferItem");

            migrationBuilder.DropForeignKey(
                name: "FK_TransferItem_Unit_UnitID",
                table: "TransferItem");

            migrationBuilder.DropForeignKey(
                name: "FK_TransferRecord_MainStore_MainStoreID",
                table: "TransferRecord");

            migrationBuilder.DropForeignKey(
                name: "FK_TransferRecord_Rooms_RoomId",
                table: "TransferRecord");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Unit",
                table: "Unit");

            migrationBuilder.DropPrimaryKey(
                name: "PK_TransferRecord",
                table: "TransferRecord");

            migrationBuilder.DropPrimaryKey(
                name: "PK_TransferItem",
                table: "TransferItem");

            migrationBuilder.DropPrimaryKey(
                name: "PK_PackingUnit",
                table: "PackingUnit");

            migrationBuilder.DropPrimaryKey(
                name: "PK_MainStoreItem",
                table: "MainStoreItem");

            migrationBuilder.DropPrimaryKey(
                name: "PK_MainStore",
                table: "MainStore");

            migrationBuilder.RenameTable(
                name: "Unit",
                newName: "Units");

            migrationBuilder.RenameTable(
                name: "TransferRecord",
                newName: "TransferRecords");

            migrationBuilder.RenameTable(
                name: "TransferItem",
                newName: "TransferItems");

            migrationBuilder.RenameTable(
                name: "PackingUnit",
                newName: "PackingUnits");

            migrationBuilder.RenameTable(
                name: "MainStoreItem",
                newName: "MainStoreItems");

            migrationBuilder.RenameTable(
                name: "MainStore",
                newName: "MainStores");

            migrationBuilder.RenameColumn(
                name: "StateModifiedDate",
                table: "OrderRoom",
                newName: "StatusModifiedDate");

            migrationBuilder.RenameIndex(
                name: "IX_TransferRecord_RoomId",
                table: "TransferRecords",
                newName: "IX_TransferRecords_RoomId");

            migrationBuilder.RenameIndex(
                name: "IX_TransferRecord_MainStoreID",
                table: "TransferRecords",
                newName: "IX_TransferRecords_MainStoreID");

            migrationBuilder.RenameIndex(
                name: "IX_TransferItem_UnitID",
                table: "TransferItems",
                newName: "IX_TransferItems_UnitID");

            migrationBuilder.RenameIndex(
                name: "IX_TransferItem_TransferRecordId",
                table: "TransferItems",
                newName: "IX_TransferItems_TransferRecordId");

            migrationBuilder.RenameIndex(
                name: "IX_TransferItem_ItemId",
                table: "TransferItems",
                newName: "IX_TransferItems_ItemId");

            migrationBuilder.RenameIndex(
                name: "IX_PackingUnit_UnitID",
                table: "PackingUnits",
                newName: "IX_PackingUnits_UnitID");

            migrationBuilder.RenameIndex(
                name: "IX_PackingUnit_ItemId",
                table: "PackingUnits",
                newName: "IX_PackingUnits_ItemId");

            migrationBuilder.RenameIndex(
                name: "IX_MainStoreItem_MainStoreId",
                table: "MainStoreItems",
                newName: "IX_MainStoreItems_MainStoreId");

            migrationBuilder.RenameIndex(
                name: "IX_MainStoreItem_ItemId",
                table: "MainStoreItems",
                newName: "IX_MainStoreItems_ItemId");

            migrationBuilder.AddColumn<string>(
                name: "CreatedBy",
                table: "Services",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedOn",
                table: "Services",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<bool>(
                name: "IsDelete",
                table: "Services",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "ModifiedBy",
                table: "Services",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ModifiedOn",
                table: "Services",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "CreatedBy",
                table: "RoomTypes",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedOn",
                table: "RoomTypes",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<bool>(
                name: "IsDelete",
                table: "RoomTypes",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "ModifiedBy",
                table: "RoomTypes",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ModifiedOn",
                table: "RoomTypes",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "CreatedBy",
                table: "Rooms",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedOn",
                table: "Rooms",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<bool>(
                name: "IsDelete",
                table: "Rooms",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "ModifiedBy",
                table: "Rooms",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ModifiedOn",
                table: "Rooms",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddPrimaryKey(
                name: "PK_Units",
                table: "Units",
                column: "ID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_TransferRecords",
                table: "TransferRecords",
                column: "ID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_TransferItems",
                table: "TransferItems",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_PackingUnits",
                table: "PackingUnits",
                column: "ID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_MainStoreItems",
                table: "MainStoreItems",
                column: "ID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_MainStores",
                table: "MainStores",
                column: "ID");

            migrationBuilder.CreateTable(
                name: "Counters",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Type = table.Column<int>(nullable: false),
                    Count = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Counters", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MainStoreStockRecords",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    MainStoreId = table.Column<Guid>(nullable: false),
                    ItemId = table.Column<Guid>(nullable: false),
                    StockDate = table.Column<DateTime>(nullable: false),
                    ExpiredDate = table.Column<DateTime>(nullable: true),
                    Quantity = table.Column<int>(nullable: false),
                    UnitID = table.Column<Guid>(nullable: true),
                    QuantityInSmallestUnit = table.Column<int>(nullable: false),
                    UnitName = table.Column<string>(nullable: true),
                    IsOutOfStock = table.Column<bool>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedBy = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MainStoreStockRecords", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MainStoreStockRecords_Items_ItemId",
                        column: x => x.ItemId,
                        principalTable: "Items",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MainStoreStockRecords_MainStores_MainStoreId",
                        column: x => x.MainStoreId,
                        principalTable: "MainStores",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MainStoreStockRecords_Units_UnitID",
                        column: x => x.UnitID,
                        principalTable: "Units",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "RoomItems",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    RoomId = table.Column<Guid>(nullable: false),
                    ItemId = table.Column<Guid>(nullable: false),
                    Quantity = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RoomItems", x => x.ID);
                    table.ForeignKey(
                        name: "FK_RoomItems_Items_ItemId",
                        column: x => x.ItemId,
                        principalTable: "Items",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RoomItems_Rooms_RoomId",
                        column: x => x.RoomId,
                        principalTable: "Rooms",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MainStoreStockRecords_ItemId",
                table: "MainStoreStockRecords",
                column: "ItemId");

            migrationBuilder.CreateIndex(
                name: "IX_MainStoreStockRecords_MainStoreId",
                table: "MainStoreStockRecords",
                column: "MainStoreId");

            migrationBuilder.CreateIndex(
                name: "IX_MainStoreStockRecords_UnitID",
                table: "MainStoreStockRecords",
                column: "UnitID");

            migrationBuilder.CreateIndex(
                name: "IX_RoomItems_ItemId",
                table: "RoomItems",
                column: "ItemId");

            migrationBuilder.CreateIndex(
                name: "IX_RoomItems_RoomId",
                table: "RoomItems",
                column: "RoomId");

            migrationBuilder.AddForeignKey(
                name: "FK_MainStoreItems_Items_ItemId",
                table: "MainStoreItems",
                column: "ItemId",
                principalTable: "Items",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_MainStoreItems_MainStores_MainStoreId",
                table: "MainStoreItems",
                column: "MainStoreId",
                principalTable: "MainStores",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PackingUnits_Items_ItemId",
                table: "PackingUnits",
                column: "ItemId",
                principalTable: "Items",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PackingUnits_Units_UnitID",
                table: "PackingUnits",
                column: "UnitID",
                principalTable: "Units",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TransferItems_Items_ItemId",
                table: "TransferItems",
                column: "ItemId",
                principalTable: "Items",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TransferItems_TransferRecords_TransferRecordId",
                table: "TransferItems",
                column: "TransferRecordId",
                principalTable: "TransferRecords",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TransferItems_Units_UnitID",
                table: "TransferItems",
                column: "UnitID",
                principalTable: "Units",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TransferRecords_MainStores_MainStoreID",
                table: "TransferRecords",
                column: "MainStoreID",
                principalTable: "MainStores",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TransferRecords_Rooms_RoomId",
                table: "TransferRecords",
                column: "RoomId",
                principalTable: "Rooms",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MainStoreItems_Items_ItemId",
                table: "MainStoreItems");

            migrationBuilder.DropForeignKey(
                name: "FK_MainStoreItems_MainStores_MainStoreId",
                table: "MainStoreItems");

            migrationBuilder.DropForeignKey(
                name: "FK_PackingUnits_Items_ItemId",
                table: "PackingUnits");

            migrationBuilder.DropForeignKey(
                name: "FK_PackingUnits_Units_UnitID",
                table: "PackingUnits");

            migrationBuilder.DropForeignKey(
                name: "FK_TransferItems_Items_ItemId",
                table: "TransferItems");

            migrationBuilder.DropForeignKey(
                name: "FK_TransferItems_TransferRecords_TransferRecordId",
                table: "TransferItems");

            migrationBuilder.DropForeignKey(
                name: "FK_TransferItems_Units_UnitID",
                table: "TransferItems");

            migrationBuilder.DropForeignKey(
                name: "FK_TransferRecords_MainStores_MainStoreID",
                table: "TransferRecords");

            migrationBuilder.DropForeignKey(
                name: "FK_TransferRecords_Rooms_RoomId",
                table: "TransferRecords");

            migrationBuilder.DropTable(
                name: "Counters");

            migrationBuilder.DropTable(
                name: "MainStoreStockRecords");

            migrationBuilder.DropTable(
                name: "RoomItems");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Units",
                table: "Units");

            migrationBuilder.DropPrimaryKey(
                name: "PK_TransferRecords",
                table: "TransferRecords");

            migrationBuilder.DropPrimaryKey(
                name: "PK_TransferItems",
                table: "TransferItems");

            migrationBuilder.DropPrimaryKey(
                name: "PK_PackingUnits",
                table: "PackingUnits");

            migrationBuilder.DropPrimaryKey(
                name: "PK_MainStores",
                table: "MainStores");

            migrationBuilder.DropPrimaryKey(
                name: "PK_MainStoreItems",
                table: "MainStoreItems");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "Services");

            migrationBuilder.DropColumn(
                name: "CreatedOn",
                table: "Services");

            migrationBuilder.DropColumn(
                name: "IsDelete",
                table: "Services");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "Services");

            migrationBuilder.DropColumn(
                name: "ModifiedOn",
                table: "Services");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "RoomTypes");

            migrationBuilder.DropColumn(
                name: "CreatedOn",
                table: "RoomTypes");

            migrationBuilder.DropColumn(
                name: "IsDelete",
                table: "RoomTypes");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "RoomTypes");

            migrationBuilder.DropColumn(
                name: "ModifiedOn",
                table: "RoomTypes");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "Rooms");

            migrationBuilder.DropColumn(
                name: "CreatedOn",
                table: "Rooms");

            migrationBuilder.DropColumn(
                name: "IsDelete",
                table: "Rooms");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "Rooms");

            migrationBuilder.DropColumn(
                name: "ModifiedOn",
                table: "Rooms");

            migrationBuilder.RenameTable(
                name: "Units",
                newName: "Unit");

            migrationBuilder.RenameTable(
                name: "TransferRecords",
                newName: "TransferRecord");

            migrationBuilder.RenameTable(
                name: "TransferItems",
                newName: "TransferItem");

            migrationBuilder.RenameTable(
                name: "PackingUnits",
                newName: "PackingUnit");

            migrationBuilder.RenameTable(
                name: "MainStores",
                newName: "MainStore");

            migrationBuilder.RenameTable(
                name: "MainStoreItems",
                newName: "MainStoreItem");

            migrationBuilder.RenameColumn(
                name: "StatusModifiedDate",
                table: "OrderRoom",
                newName: "StateModifiedDate");

            migrationBuilder.RenameIndex(
                name: "IX_TransferRecords_RoomId",
                table: "TransferRecord",
                newName: "IX_TransferRecord_RoomId");

            migrationBuilder.RenameIndex(
                name: "IX_TransferRecords_MainStoreID",
                table: "TransferRecord",
                newName: "IX_TransferRecord_MainStoreID");

            migrationBuilder.RenameIndex(
                name: "IX_TransferItems_UnitID",
                table: "TransferItem",
                newName: "IX_TransferItem_UnitID");

            migrationBuilder.RenameIndex(
                name: "IX_TransferItems_TransferRecordId",
                table: "TransferItem",
                newName: "IX_TransferItem_TransferRecordId");

            migrationBuilder.RenameIndex(
                name: "IX_TransferItems_ItemId",
                table: "TransferItem",
                newName: "IX_TransferItem_ItemId");

            migrationBuilder.RenameIndex(
                name: "IX_PackingUnits_UnitID",
                table: "PackingUnit",
                newName: "IX_PackingUnit_UnitID");

            migrationBuilder.RenameIndex(
                name: "IX_PackingUnits_ItemId",
                table: "PackingUnit",
                newName: "IX_PackingUnit_ItemId");

            migrationBuilder.RenameIndex(
                name: "IX_MainStoreItems_MainStoreId",
                table: "MainStoreItem",
                newName: "IX_MainStoreItem_MainStoreId");

            migrationBuilder.RenameIndex(
                name: "IX_MainStoreItems_ItemId",
                table: "MainStoreItem",
                newName: "IX_MainStoreItem_ItemId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Unit",
                table: "Unit",
                column: "ID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_TransferRecord",
                table: "TransferRecord",
                column: "ID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_TransferItem",
                table: "TransferItem",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_PackingUnit",
                table: "PackingUnit",
                column: "ID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_MainStore",
                table: "MainStore",
                column: "ID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_MainStoreItem",
                table: "MainStoreItem",
                column: "ID");

            migrationBuilder.AddForeignKey(
                name: "FK_MainStoreItem_Items_ItemId",
                table: "MainStoreItem",
                column: "ItemId",
                principalTable: "Items",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_MainStoreItem_MainStore_MainStoreId",
                table: "MainStoreItem",
                column: "MainStoreId",
                principalTable: "MainStore",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PackingUnit_Items_ItemId",
                table: "PackingUnit",
                column: "ItemId",
                principalTable: "Items",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PackingUnit_Unit_UnitID",
                table: "PackingUnit",
                column: "UnitID",
                principalTable: "Unit",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TransferItem_Items_ItemId",
                table: "TransferItem",
                column: "ItemId",
                principalTable: "Items",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TransferItem_TransferRecord_TransferRecordId",
                table: "TransferItem",
                column: "TransferRecordId",
                principalTable: "TransferRecord",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TransferItem_Unit_UnitID",
                table: "TransferItem",
                column: "UnitID",
                principalTable: "Unit",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TransferRecord_MainStore_MainStoreID",
                table: "TransferRecord",
                column: "MainStoreID",
                principalTable: "MainStore",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TransferRecord_Rooms_RoomId",
                table: "TransferRecord",
                column: "RoomId",
                principalTable: "Rooms",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
