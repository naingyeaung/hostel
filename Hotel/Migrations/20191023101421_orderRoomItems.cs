﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Hotel.Migrations
{
    public partial class orderRoomItems : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_RoomItems_Units_UnitID",
                table: "RoomItems");

            migrationBuilder.DropIndex(
                name: "IX_RoomItems_UnitID",
                table: "RoomItems");

            migrationBuilder.DropColumn(
                name: "QuantityInSmallestUnit",
                table: "RoomItems");

            migrationBuilder.DropColumn(
                name: "UnitID",
                table: "RoomItems");

            migrationBuilder.DropColumn(
                name: "UnitName",
                table: "RoomItems");

            migrationBuilder.DropColumn(
                name: "UnitPrice",
                table: "RoomItems");

            migrationBuilder.RenameColumn(
                name: "Qty",
                table: "OrderRoomItems",
                newName: "QuantityInSmallestUnit");

            migrationBuilder.RenameColumn(
                name: "PricePerItem",
                table: "OrderRoomItems",
                newName: "UnitPrice");

            migrationBuilder.AddColumn<int>(
                name: "Quantity",
                table: "OrderRoomItems",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<Guid>(
                name: "UnitID",
                table: "OrderRoomItems",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<string>(
                name: "UnitName",
                table: "OrderRoomItems",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_OrderRoomItems_UnitID",
                table: "OrderRoomItems",
                column: "UnitID");

            migrationBuilder.AddForeignKey(
                name: "FK_OrderRoomItems_Units_UnitID",
                table: "OrderRoomItems",
                column: "UnitID",
                principalTable: "Units",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_OrderRoomItems_Units_UnitID",
                table: "OrderRoomItems");

            migrationBuilder.DropIndex(
                name: "IX_OrderRoomItems_UnitID",
                table: "OrderRoomItems");

            migrationBuilder.DropColumn(
                name: "Quantity",
                table: "OrderRoomItems");

            migrationBuilder.DropColumn(
                name: "UnitID",
                table: "OrderRoomItems");

            migrationBuilder.DropColumn(
                name: "UnitName",
                table: "OrderRoomItems");

            migrationBuilder.RenameColumn(
                name: "UnitPrice",
                table: "OrderRoomItems",
                newName: "PricePerItem");

            migrationBuilder.RenameColumn(
                name: "QuantityInSmallestUnit",
                table: "OrderRoomItems",
                newName: "Qty");

            migrationBuilder.AddColumn<int>(
                name: "QuantityInSmallestUnit",
                table: "RoomItems",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<Guid>(
                name: "UnitID",
                table: "RoomItems",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<string>(
                name: "UnitName",
                table: "RoomItems",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "UnitPrice",
                table: "RoomItems",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.CreateIndex(
                name: "IX_RoomItems_UnitID",
                table: "RoomItems",
                column: "UnitID");

            migrationBuilder.AddForeignKey(
                name: "FK_RoomItems_Units_UnitID",
                table: "RoomItems",
                column: "UnitID",
                principalTable: "Units",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
