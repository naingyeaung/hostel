﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Hotel.Migrations
{
    public partial class roomtypeitem : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Qty",
                table: "RoomTypeItems",
                newName: "QuantityInSmallestUnit");

            migrationBuilder.AddColumn<int>(
                name: "Quantity",
                table: "RoomTypeItems",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<Guid>(
                name: "UnitID",
                table: "RoomTypeItems",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<string>(
                name: "UnitName",
                table: "RoomTypeItems",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "UnitPrice",
                table: "RoomTypeItems",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.CreateIndex(
                name: "IX_RoomTypeItems_UnitID",
                table: "RoomTypeItems",
                column: "UnitID");

            migrationBuilder.AddForeignKey(
                name: "FK_RoomTypeItems_Units_UnitID",
                table: "RoomTypeItems",
                column: "UnitID",
                principalTable: "Units",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_RoomTypeItems_Units_UnitID",
                table: "RoomTypeItems");

            migrationBuilder.DropIndex(
                name: "IX_RoomTypeItems_UnitID",
                table: "RoomTypeItems");

            migrationBuilder.DropColumn(
                name: "Quantity",
                table: "RoomTypeItems");

            migrationBuilder.DropColumn(
                name: "UnitID",
                table: "RoomTypeItems");

            migrationBuilder.DropColumn(
                name: "UnitName",
                table: "RoomTypeItems");

            migrationBuilder.DropColumn(
                name: "UnitPrice",
                table: "RoomTypeItems");

            migrationBuilder.RenameColumn(
                name: "QuantityInSmallestUnit",
                table: "RoomTypeItems",
                newName: "Qty");
        }
    }
}
