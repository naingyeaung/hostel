﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Hotel.Migrations
{
    public partial class roomItemUnit : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "QuantityInSmallestUnit",
                table: "RoomItems",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<Guid>(
                name: "UnitID",
                table: "RoomItems",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<string>(
                name: "UnitName",
                table: "RoomItems",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "UnitPrice",
                table: "RoomItems",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.CreateIndex(
                name: "IX_RoomItems_UnitID",
                table: "RoomItems",
                column: "UnitID");

            migrationBuilder.AddForeignKey(
                name: "FK_RoomItems_Units_UnitID",
                table: "RoomItems",
                column: "UnitID",
                principalTable: "Units",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_RoomItems_Units_UnitID",
                table: "RoomItems");

            migrationBuilder.DropIndex(
                name: "IX_RoomItems_UnitID",
                table: "RoomItems");

            migrationBuilder.DropColumn(
                name: "QuantityInSmallestUnit",
                table: "RoomItems");

            migrationBuilder.DropColumn(
                name: "UnitID",
                table: "RoomItems");

            migrationBuilder.DropColumn(
                name: "UnitName",
                table: "RoomItems");

            migrationBuilder.DropColumn(
                name: "UnitPrice",
                table: "RoomItems");
        }
    }
}
