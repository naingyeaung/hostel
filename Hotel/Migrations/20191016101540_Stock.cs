﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Hotel.Migrations
{
    public partial class Stock : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "OrderNo",
                table: "Orders",
                newName: "VoucherNo");

            migrationBuilder.RenameColumn(
                name: "Price",
                table: "Items",
                newName: "UnitPrice");

            migrationBuilder.AddColumn<string>(
                name: "Code",
                table: "Items",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CreatedBy",
                table: "Items",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedOn",
                table: "Items",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<bool>(
                name: "IsDelete",
                table: "Items",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "ModifiedBy",
                table: "Items",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ModifiedOn",
                table: "Items",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<int>(
                name: "PercentageForSale",
                table: "Items",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "MainStore",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Address = table.Column<string>(nullable: true),
                    Township = table.Column<string>(nullable: true),
                    City = table.Column<string>(nullable: true),
                    Remark = table.Column<string>(nullable: true),
                    IsDelete = table.Column<bool>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedBy = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MainStore", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Unit",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    Description = table.Column<string>(nullable: false),
                    UnitOrder = table.Column<int>(nullable: false),
                    Remark = table.Column<string>(nullable: true),
                    IsDelete = table.Column<bool>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedBy = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Unit", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "MainStoreItem",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    MainStoreId = table.Column<Guid>(nullable: false),
                    ItemId = table.Column<Guid>(nullable: false),
                    Quantity = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MainStoreItem", x => x.ID);
                    table.ForeignKey(
                        name: "FK_MainStoreItem_Items_ItemId",
                        column: x => x.ItemId,
                        principalTable: "Items",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MainStoreItem_MainStore_MainStoreId",
                        column: x => x.MainStoreId,
                        principalTable: "MainStore",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TransferRecord",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    MainStoreID = table.Column<Guid>(nullable: false),
                    RoomId = table.Column<Guid>(nullable: false),
                    TransferDate = table.Column<DateTime>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedBy = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TransferRecord", x => x.ID);
                    table.ForeignKey(
                        name: "FK_TransferRecord_MainStore_MainStoreID",
                        column: x => x.MainStoreID,
                        principalTable: "MainStore",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TransferRecord_Rooms_RoomId",
                        column: x => x.RoomId,
                        principalTable: "Rooms",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PackingUnit",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    ItemId = table.Column<Guid>(nullable: false),
                    UnitID = table.Column<Guid>(nullable: false),
                    QuantityInParent = table.Column<int>(nullable: false),
                    PurchasedAmount = table.Column<decimal>(nullable: false),
                    SaleAmount = table.Column<decimal>(nullable: false),
                    UnitOrder = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PackingUnit", x => x.ID);
                    table.ForeignKey(
                        name: "FK_PackingUnit_Items_ItemId",
                        column: x => x.ItemId,
                        principalTable: "Items",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PackingUnit_Unit_UnitID",
                        column: x => x.UnitID,
                        principalTable: "Unit",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TransferItem",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    TransferRecordId = table.Column<Guid>(nullable: false),
                    ItemId = table.Column<Guid>(nullable: false),
                    Quantity = table.Column<int>(nullable: false),
                    UnitID = table.Column<Guid>(nullable: false),
                    QuantityInSmallestUnit = table.Column<int>(nullable: false),
                    UnitName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TransferItem", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TransferItem_Items_ItemId",
                        column: x => x.ItemId,
                        principalTable: "Items",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TransferItem_TransferRecord_TransferRecordId",
                        column: x => x.TransferRecordId,
                        principalTable: "TransferRecord",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TransferItem_Unit_UnitID",
                        column: x => x.UnitID,
                        principalTable: "Unit",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MainStoreItem_ItemId",
                table: "MainStoreItem",
                column: "ItemId");

            migrationBuilder.CreateIndex(
                name: "IX_MainStoreItem_MainStoreId",
                table: "MainStoreItem",
                column: "MainStoreId");

            migrationBuilder.CreateIndex(
                name: "IX_PackingUnit_ItemId",
                table: "PackingUnit",
                column: "ItemId");

            migrationBuilder.CreateIndex(
                name: "IX_PackingUnit_UnitID",
                table: "PackingUnit",
                column: "UnitID");

            migrationBuilder.CreateIndex(
                name: "IX_TransferItem_ItemId",
                table: "TransferItem",
                column: "ItemId");

            migrationBuilder.CreateIndex(
                name: "IX_TransferItem_TransferRecordId",
                table: "TransferItem",
                column: "TransferRecordId");

            migrationBuilder.CreateIndex(
                name: "IX_TransferItem_UnitID",
                table: "TransferItem",
                column: "UnitID");

            migrationBuilder.CreateIndex(
                name: "IX_TransferRecord_MainStoreID",
                table: "TransferRecord",
                column: "MainStoreID");

            migrationBuilder.CreateIndex(
                name: "IX_TransferRecord_RoomId",
                table: "TransferRecord",
                column: "RoomId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MainStoreItem");

            migrationBuilder.DropTable(
                name: "PackingUnit");

            migrationBuilder.DropTable(
                name: "TransferItem");

            migrationBuilder.DropTable(
                name: "TransferRecord");

            migrationBuilder.DropTable(
                name: "Unit");

            migrationBuilder.DropTable(
                name: "MainStore");

            migrationBuilder.DropColumn(
                name: "Code",
                table: "Items");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "Items");

            migrationBuilder.DropColumn(
                name: "CreatedOn",
                table: "Items");

            migrationBuilder.DropColumn(
                name: "IsDelete",
                table: "Items");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "Items");

            migrationBuilder.DropColumn(
                name: "ModifiedOn",
                table: "Items");

            migrationBuilder.DropColumn(
                name: "PercentageForSale",
                table: "Items");

            migrationBuilder.RenameColumn(
                name: "VoucherNo",
                table: "Orders",
                newName: "OrderNo");

            migrationBuilder.RenameColumn(
                name: "UnitPrice",
                table: "Items",
                newName: "Price");
        }
    }
}
