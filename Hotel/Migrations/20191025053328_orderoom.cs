﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Hotel.Migrations
{
    public partial class orderoom : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_OrderRoom_Orders_OrderId",
                table: "OrderRoom");

            migrationBuilder.DropForeignKey(
                name: "FK_OrderRoom_Rooms_RoomId",
                table: "OrderRoom");

            migrationBuilder.DropForeignKey(
                name: "FK_OrderRoomItems_OrderRoom_OrderRoomId",
                table: "OrderRoomItems");

            migrationBuilder.DropForeignKey(
                name: "FK_OrderRoomServices_OrderRoom_OrderRoomId",
                table: "OrderRoomServices");

            migrationBuilder.DropPrimaryKey(
                name: "PK_OrderRoom",
                table: "OrderRoom");

            migrationBuilder.RenameTable(
                name: "OrderRoom",
                newName: "OrderRooms");

            migrationBuilder.RenameIndex(
                name: "IX_OrderRoom_RoomId",
                table: "OrderRooms",
                newName: "IX_OrderRooms_RoomId");

            migrationBuilder.RenameIndex(
                name: "IX_OrderRoom_OrderId",
                table: "OrderRooms",
                newName: "IX_OrderRooms_OrderId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_OrderRooms",
                table: "OrderRooms",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_OrderRoomItems_OrderRooms_OrderRoomId",
                table: "OrderRoomItems",
                column: "OrderRoomId",
                principalTable: "OrderRooms",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_OrderRooms_Orders_OrderId",
                table: "OrderRooms",
                column: "OrderId",
                principalTable: "Orders",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_OrderRooms_Rooms_RoomId",
                table: "OrderRooms",
                column: "RoomId",
                principalTable: "Rooms",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_OrderRoomServices_OrderRooms_OrderRoomId",
                table: "OrderRoomServices",
                column: "OrderRoomId",
                principalTable: "OrderRooms",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_OrderRoomItems_OrderRooms_OrderRoomId",
                table: "OrderRoomItems");

            migrationBuilder.DropForeignKey(
                name: "FK_OrderRooms_Orders_OrderId",
                table: "OrderRooms");

            migrationBuilder.DropForeignKey(
                name: "FK_OrderRooms_Rooms_RoomId",
                table: "OrderRooms");

            migrationBuilder.DropForeignKey(
                name: "FK_OrderRoomServices_OrderRooms_OrderRoomId",
                table: "OrderRoomServices");

            migrationBuilder.DropPrimaryKey(
                name: "PK_OrderRooms",
                table: "OrderRooms");

            migrationBuilder.RenameTable(
                name: "OrderRooms",
                newName: "OrderRoom");

            migrationBuilder.RenameIndex(
                name: "IX_OrderRooms_RoomId",
                table: "OrderRoom",
                newName: "IX_OrderRoom_RoomId");

            migrationBuilder.RenameIndex(
                name: "IX_OrderRooms_OrderId",
                table: "OrderRoom",
                newName: "IX_OrderRoom_OrderId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_OrderRoom",
                table: "OrderRoom",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_OrderRoom_Orders_OrderId",
                table: "OrderRoom",
                column: "OrderId",
                principalTable: "Orders",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_OrderRoom_Rooms_RoomId",
                table: "OrderRoom",
                column: "RoomId",
                principalTable: "Rooms",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_OrderRoomItems_OrderRoom_OrderRoomId",
                table: "OrderRoomItems",
                column: "OrderRoomId",
                principalTable: "OrderRoom",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_OrderRoomServices_OrderRoom_OrderRoomId",
                table: "OrderRoomServices",
                column: "OrderRoomId",
                principalTable: "OrderRoom",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
